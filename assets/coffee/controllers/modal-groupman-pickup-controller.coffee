module.exports = [
    '$scope', '$uibModalInstance', 'new_group', 'manscore_list',
    ($scope, $uibModalInstance, new_group, manscore_list) ->
        $scope.new_group = new_group
        $scope.manscore_list = manscore_list

        $scope.cancel = () ->
            $uibModalInstance.dismiss('cancel')

        $scope.toggleManscoreChecked = (man) ->
            if man.checked
                return man.checked = not man.checked
            man.checked = true

        $scope.addGroupMan = (new_group, manscore_list) ->
            list = _.filter(manscore_list, (man) ->
                return man.checked == true
            )
            if not new_group.group_man
                new_group.group_man = []
            _.forEach(list, (man) ->
                new_group.group_man.push man
            )

            $uibModalInstance.close(new_group)
]