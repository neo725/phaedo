module.exports = ['$rootScope', '$scope', '$uibModal', '$log', '$timeout', 'msgbox', 'api',
    ($rootScope, $scope, $uibModal, $log, $timeout, msgbox, api) ->
        $scope.exhibition = {}
        $scope.exhibition_items = []

        $rootScope.is_no_banner = true

        $scope.gotoExhibitionItem = (exhibition_item) ->
            $log.info "goto exhibition_item_no : #{exhibition_item.exhibition_item_no}"
            modalInstance = $uibModal.open(
                animation: true
                size: 'lg'
                windowClass: 'modal-exhibition-item'
                templateUrl: 'modalExhibitionItem.html'
                controller: 'ModalExhibitionItemController'
                resolve:
                    exhibition_item: ->
                        return exhibition_item
            )

        loadExhibitionItems = (exhibition_no, success_fn, error_fn) ->
            onSuccess = (response) ->
                $scope.exhibition_no = exhibition_no
                $scope.exhibition_items = response.exhibition_items

                if success_fn
                    success_fn()

            onError = () ->
                if error_fn
                    error_fn()

            api.getExhibitionItems(exhibition_no, onSuccess, onError)

        loadExhibitionList = () ->
            onSuccess = (response) ->
                exhibitions = response

                if exhibitions.length > 0
                    exhibition = exhibitions[0]
                    exhibition_no = exhibition.exhibition_no
                    $scope.exhibition = exhibition

                    onSuccess = () ->
                        $timeout(() ->
                            $scope.exhibition_loaded = true
                            $scope.show_exhibitions = true
                        , 1000)

                    loadExhibitionItems(exhibition_no, onSuccess, (->))
            onError = (->)

            api.getExhibitions(onSuccess, onError)

        loadExhibitionList()
]