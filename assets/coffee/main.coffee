resources = require('./translation/index')()
constants = require('./common/constants')
directives = require('./common/directives')

angular.module('phaedo', ['ui.router', 'ngCookies', 'pascalprecht.translate', 'ngFileUpload', 'angular-cache',
    'ui.bootstrap', 'angularGrid', 'SignalR', 'angularMoment', 'slickCarousel', 'oitozero.ngSweetAlert', 'ui.select', 'ngSanitize', 'textAngular',
    'mwl.calendar', 'ui.bootstrap.datetimepicker', 'ui.dateTimeInput'])
.factory('api', require('./common/api-service'))
.factory('msgbox', require('./common/messagebox'))
.factory('FakeJsonService', require('./common/fake-json-service'))
.factory('UserService', require('./common/user-service'))
.factory('CourseService', require('./common/course-service'))
.factory('ManscoreService', require('./common/manscore-service'))
.factory('ProjectService', require('./common/project-service'))
.factory('ChatHubService', require('./common/chathub-service'))
.factory('LikeHubService', require('./common/likehub-service'))
.factory('HeartbeatHubService', require('./common/heartbeathub-service'))
.factory('httpInterceptor', require('./common/http-interceptors'))
.directive('ngEnter', directives.ngEnter)
.directive('pdRemoveClassWhenReady', directives.pdRemoveClassWhenReady)
.directive('pdChatAtUsers', directives.pdChatAtUsers)
.directive('pdScrollToBottom', directives.pdScrollToBottom)
.directive('pdSignalrHubScript', directives.pdSignalrHubScript)
.directive('saveEnter', directives.saveEnter)
.directive('focusIf', directives.focusIf)
.directive('pdChat', require('./common/directive-chat'))
.controller('IndexController', require('./controllers/index-controller'))
.controller('PublicController', require('./controllers/public-controller'))
.controller('HomeController', require('./controllers/home-controller'))
.controller('CourseListController', require('./controllers/course-list-controller'))
.controller('UserDropdownController', require('./controllers/user-dropdown-controller'))
.controller('NotifyChatDropdownController', require('./controllers/notify-chat-dropdown-controller'))
.controller('CalendarDropdownController', require('./controllers/calendar-dropdown-controller'))
.controller('LoginController', require('./controllers/login-controller'))
.controller('ProjectListController', require('./controllers/project-list-controller'))
.controller('ProjectInfoController', require('./controllers/project-info-controller'))
.controller('PublicWallController', require('./controllers/publicwall-controller'))
.controller('AirController', require('./controllers/air-controller'))
.controller('WXLoginController', require('./controllers/wxlogin-controller'))
.controller('ModalGroupInfoController', require('./controllers/modal-groupinfo-controller'))
.controller('ModalGroupManPickupController', require('./controllers/modal-groupman-pickup-controller'))
.controller('ModalProjectGroupController', require('./controllers/modal-project-group-controller'))
.controller('ModalHomeworkController', require('./controllers/modal-homework-controller'))
.controller('ModalMyHomeworkController', require('./controllers/modal-my-homework-controller'))
.controller('ModalMyNoteController', require('./controllers/modal-my-note-controller'))
.controller('ModalExhibitionFormController', require('./controllers/modal-exhibition-form-controller'))
.controller('ModalExhibitionItemFormController', require('./controllers/modal-exhibition-item-form-controller'))
.controller('ModalExhibitionItemController', require('./controllers/modal-exhibition-item-controller'))
.controller('ModalPublicWallItemController', require('./controllers/modal-publicwall-item-controller'))
.controller('MyNoteController', require('./controllers/my-note-controller'))
.controller('NotifyModalController', require('./controllers/notify-modal-controller'))
.controller('CalendarModalController', require('./controllers/calendar-modal-controller'))
.controller('CreateChatModalController', require('./controllers/create-chat-modal-controller'))
.controller('ChatModalController', require('./controllers/chat-modal-controller'))
.controller('ExhibitionListController', require('./controllers/exhibition-list-controller'))
.controller('ExhibitionController', require('./controllers/exhibition-controller'))
.controller('ExhibitionItemController', require('./controllers/exhibition-item-controller'))
.run(require('./common/heartbeat-service'))
.run((amMoment) ->
    amMoment.changeLocale('zh-cn')
)
.config(['$httpProvider', ($httpProvider) ->
    $httpProvider.defaults.timeout = 5000

    $httpProvider.defaults.useXDomain = true
    delete $httpProvider.defaults.headers.common['X-Requested-With']

    $httpProvider.defaults.headers.patch =
        'Content-Type': 'application/json; charset=utf-8'

    $httpProvider.interceptors.push('httpInterceptor')
])
.config(['$translateProvider', ($translateProvider) ->
    for language of resources
        $translateProvider.translations(language, resources[language])

    $translateProvider.use constants.DEFAULT_LOCALE
])
.config(['CacheFactoryProvider', (CacheFactoryProvider) ->
    angular.extend(CacheFactoryProvider.defaults,
        storageMode: 'sessionStorage'
    )
])
.config(['calendarConfig', (calendarConfig) ->
    calendarConfig.dateFormatter = 'moment'
    moment.locale('zh-cn', {
        week : {
            dow : 0 # Sunday is the first day of the week
        }
    })
])
.config(['$stateProvider', '$urlRouterProvider', ($stateProvider, $urlRouterProvider) ->

    $stateProvider

    ## misc (before login)
    .state 'login',
        url: '/login'
        templateUrl: 'partial/_login.html'

    ## public (before login)
    .state 'public',
        url: '/public'
        abstract: true
        templateUrl: 'partial/public.html'

    .state 'public.public-wall',
        url: '/public-wall'
        views:
            'publicContent':
                templateUrl: 'partial/_public-wall.html'

    .state 'public.exhibition-list',
        url: '/exhibition'
        views:
            'publicContent':
                templateUrl: 'partial/_exhibition-list.html'

#    .state 'public.exhibition',
#        url: '/exhibition/:exhibition_no'
#        views:
#            'publicContent':
#                templateUrl: 'partial/_exhibition.html'
    .state 'public.exhibition-item',
        url: '/exhibition/:exhibition_no/:exhibition_item_no'
        views:
            'publicContent':
                templateUrl: 'partial/_exhibition-item.html'

    .state 'public.air',
        url: '/air'
        views:
            'publicContent':
                templateUrl: 'partial/_air.html'

    .state 'public.air-apply',
        url: '/air-apply'
        views:
            'publicContent':
                templateUrl: 'partial/_air-apply.html'

    ## home (after login)
    .state 'home',
        url: '/home'
        abstract: true
        templateUrl: 'partial/home.html'

    .state 'home.course-list',
        url: '/'
        views:
            'homeContent':
                templateUrl: 'partial/_course-list.html'

    .state 'home.archive',
        url: '/archive'
        views:
            'homeContent':
                templateUrl: 'partial/_course-list.html'

    .state 'home.project-list',
        url: '/project-list'
        views:
            'homeContent':
                templateUrl: 'partial/_project-list.html'

    .state 'home.project-info',
        url: '/project-info'
        views:
            'homeContent':
                templateUrl: 'partial/_project.html'

    .state 'home.my-note',
        url: '/my-note'
        views:
            'homeContent':
                templateUrl: 'partial/_my-note.html'

    $urlRouterProvider.otherwise '/home/'

])