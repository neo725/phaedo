module.exports = {
    'jquery': {
        exports: 'global:$'
    },
    'angular': {
        exports: 'global:angular',
        depends: {
            'jquery': '$'
        }
    },
    'bootstrap': {
        exports: 'global:bootstrap',
        depends: {
            'jquery': '$'
        }
    },
    'interactjs': {
        exports: 'global:interact'
    },
    'moment': {
        exports: 'global:moment'
    },
    'angular-ui-router': {
        exports: 'global:angular-ui-router'
    },
    'angular-ui-bootstrap': {
        exports: 'global:angular-ui-bootstrap'
    },
    'angular-ui-bootstrap-tpls': {
        exports: 'global:angular-ui-bootstrap-tpls'
    },
    'lodash': {
        exports: 'global:_'
    },
    'sweetalert': {
        exports: 'global:swal'
    },
    'angular-ui-select': {
        exports: 'global:angular-ui-select',
        depends: {
            'angular': 'angular'
        }
    },
    'textAngular': {
        exports: 'global:textAngular',
        depends: {
            'angular': 'angular'
        }
    }
    /*,
    'google-marker-with-label': {
        exports: 'MarkerWithLabel'
    }*/
};