constants = require('../common/constants')

module.exports = ['$rootScope', '$scope', '$state', '$sce', '$timeout', '$log', 'UserService', 'ChatHubService',
    ($rootScope, $scope, $state, $sce, $timeout, $log, UserService, ChatHubService) ->

        $rootScope.is_no_banner = false
        $rootScope.stay_on_top = false

        listeners = {
            'helloBack': (name) ->
                $log.info(name)
                d = new Date()
                $log.info d
        }

        state_listeners = {
            state: ''
            'connecting': (() ->)
            'connected': () ->
                this.state = 'connected'
                $log.info 'connected.'
                $rootScope.$broadcast('pd:signalr:chathub:connected')
            reconnecting: () ->
                this.state = 'reconnecting'
            'disconnected': () ->
                this.state = 'disconnected'
                $log.info 'disconnected.'
                $timeout(runConnect, 3000)
        }

        runConnect = () ->
            ChatHubService.connect(listeners, state_listeners)

        runConnect()

        $scope.$on '$stateChangeSuccess', ->
            $scope.user = UserService.get()
]