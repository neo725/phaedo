module.exports = ['$window', ($window) ->
    KEY = 'Phaedo.CurrentManscore'

    set = (course_no, data) ->
        $window.sessionStorage.setItem("#{KEY}:#{course_no}", JSON.stringify(data))

    get = (course_no) ->
        data = $window.sessionStorage.getItem("#{KEY}:#{course_no}")
        if (data)
            data = JSON.parse(data)
        return data

    return {
        get: get
        set: set
    }
]