module.exports = ->
    login = {
        "token": "abcdefg",
        "man_name": "余海",
        "school_name": "明德學院"
    }

    course_list = [
        {
            "course_no": 1,
            "course_name": "造型",
            "course_grade": 1,
            "course_image": "(Image URL)",
            "course_startdate": "2017/1/13",
            "course_enddate": "2017/3/1",
            "course_credit": 3
        },
        {
            "course_no": 2,
            "course_name": "色彩",
            "course_grade": 1,
            "course_image": "(Image URL)",
            "course_startdate": "2017/1/13",
            "course_enddate": "2017/3/1",
            "course_credit": 3
        },
        {
            "course_no": 3,
            "course_name": "影像語言分析",
            "course_grade": 2,
            "course_image": "(Image URL)",
            "course_startdate": "2017/1/13",
            "course_enddate": "2017/3/1",
            "course_credit": 3
        }
    ]


    manscore = {
        "man_no": 1,
        "man_name": "于海",
        "man_avatar": "http://...",
        "manscore_type": 2000
    }

    manscore_list = [
        {
            "man_no": 2,
            "man_name": "閻立群",
            "man_avatar": "http://...",
            "group_no": 1
        },
        {
            "man_no": 3,
            "man_name": "張琮華",
            "man_avatar": "http://...",
            "group_no": 1
        },
        {
            "man_no": 4,
            "man_name": "李家麒",
            "man_avatar": "http://...",
            "group_no": -1
        },
        {
            "man_no": 5,
            "man_name": "鄒家慶",
            "man_avatar": "",
            "group_no": -1
        },
        {
            "man_no": 6,
            "man_name": "張一",
            "man_avatar": "",
            "group_no": -1
        },
        {
            "man_no": 7,
            "man_name": "李二",
            "man_avatar": "",
            "group_no": -1
        },
        {
            "man_no": 8,
            "man_name": "鄒三",
            "man_avatar": "",
            "group_no": -1
        }
    ]

    project_list = [
        {
            "project_no": 1,
            "project_name": "植物型態研究1",
            "project_status": 10,
            "project_type": 10,
            "project_startdate": "2017/12/1",
            "project_enddate": "2017/12/31"
        },
        {
            "project_no": 2,
            "project_name": "植物型態研究2",
            "project_status": 10,
            "project_type": 10,
            "project_startdate": "2018/1/1",
            "project_enddate": "2018/3/31"
        },
        {
            "project_no": 3,
            "project_name": "教師自建專題1",
            "project_status": 10,
            "project_type": 20,
            "project_startdate": "",
            "project_enddate": ""
        },
        {
            "project_no": 4,
            "project_name": "替大師做減法",
            "project_status": 20,
            "project_type": 10,
            "project_startdate": "2017/1/1",
            "project_enddate": "2017/6/30"
        },
        {
            "project_no": 5,
            "project_name": "我的畫",
            "project_status": 30,
            "project_type": 10,
            "project_startdate": "2016/6/1",
            "project_enddate": "2016/7/31"
        },
        {
            "project_no": 6,
            "project_name": "學生自建專題1",
            "project_status": 10,
            "project_type": 30,
            "project_startdate": "",
            "project_enddate": ""
        },
        {
            "project_no": 7,
            "project_name": "學生自建專題2",
            "project_status": 10,
            "project_type": 30,
            "project_startdate": "",
            "project_enddate": ""
        }
    ]

    project = {
        "project_no": 1,
        "project_name": "造型要素及其比例研究",
        "project_startdate": "2017/1/10",
        "project_enddate": "2017/3/1",
        "project_status": 20,
        "project_type": 20,
        "project_hours": 2,
        "project_intro": "這是專題內容",
        "project_prestudy": "這是課前預告",
        "project_material": "這是關聯教材",
        "project_tags": ["標籤1", "標籤2", "標籤3"]
    }

    create_project = {
        "project_no": 1,
        "project_name": "造型要素及其比例研究",
        "project_startdate": "2017/2/1",
        "project_enddate": "2017/3/1",
        "project_status": 10,
        "project_type": 20,
        "project_hours": 0,
        "project_intro": "",
        "project_prestudy": "",
        "project_material": "",
        "project_tags": []
    }

    project_score = {
        "project_no": 1,
        "man_no": 2,
        "project_score": 90.0
    }

    group_list = [
        {
            "group_no": 1,
            "group_index": 1,
            "group_name": "立群分組1",
            "group_man": [
                {
                    "man_no": 2,
                    "man_name": "閻立群",
                    "man_avatar": "http://...",
                    "groupman_type": 10
                },
                {
                    "man_no": 3,
                    "man_name": "張琮華",
                    "man_avatar": "http://...",
                    "groupman_type": 20
                }
            ]
        }
    ]

    evaluate_list = [
        "非常好！請繼續加油",
        "光影部份要加強\r\n請明天帶作業過來"
    ]

    create_evaluate = {}

    create_group = {
        "course_no": 1,
        "project_no": 1,
        "group_no": 1
    }

    create_groupman = {
        "course_no": 1,
        "project_no": 1,
        "group_no": 2,
        "group_index": 2,
        "group_name": "家麒分組2",
        "group_man": [
            {
                "man_no": 4,
                "man_name": "李家麒",
                "man_avatar": "http://...",
                "groupman_type": 10
            }, {
                "man_no": 5,
                "man_name": "鄒家慶",
                "man_avatar": "http://...",
                "groupman_type": 20
            }
        ]
    }

    create_homework = {
        "homework_no": 1,
        "group_no": 1
    }

    homework_list = [
        {
            "homework_no": 1,
            "group_no": 1,
            "files": [
                {
                    "file_no": 1,
                    "file_name": "(file name)",
                    "file_size": 123,
                    "file_content_type": "application/octet-stream",
                    "file_download_url": "(http://...)"
                }
            ]
        },
        {
            "homework_no": 2,
            "group_no": 1,
            "files": [
                {
                    "file_no": 2,
                    "file_name": "(file name)",
                    "file_size": 123,
                    "file_content_type": "application/octet-stream",
                    "file_download_url": "(http://...)"
                }
            ]
        }
    ]

    publicwall_list = [
        {
            "wall_no": 1,
            "wall_name": "作品分享",
            "wall_desc": "用來練手的畫作，用了抽象的手法和超現實主義抽象的手法和超現實主義抽象的手法和超現實主義",
            "wall_files": [
                {
                    "file_no": 1,
                    "file_name": "wall_1.jpg",
                    "file_content_type": "image/jpeg",
                    "image_url": "(http://...)"
                }
            ]
        },
        {
            "wall_no": 1,
            "wall_name": "作品分享",
            "wall_desc": "用來練手的畫作，用了抽象的手法和超現實主義抽象的手法和超現實主義抽象的手法和超現實主義",
            "wall_files": [
                {
                    "file_no": 1,
                    "file_name": "wall_1.jpg",
                    "file_content_type": "image/jpeg",
                    "image_url": "(http://...)"
                }
            ]
        },
        {
            "wall_no": 1,
            "wall_name": "作品分享",
            "wall_desc": "用來練手的畫作，用了抽象的手法和超現實主義抽象的手法和超現實主義抽象的手法和超現實主義",
            "wall_files": [
                {
                    "file_no": 1,
                    "file_name": "wall_1.jpg",
                    "file_content_type": "image/jpeg",
                    "image_url": "(http://...)"
                }
            ]
        }
    ]

    airmap_list = [
        {
            "airmap_no": 1,
            "airmap_name": "王大明",
            "airmap_desc": "",
            "airmap_latlng_n": 25.02583,
            "airmap_latlng_e": 121.53812
        },
        {
            "airmap_no": 2,
            "airmap_name": "李強",
            "airmap_desc": "",
            "airmap_latlng_n": 25.04442,
            "airmap_latlng_e": 121.52574
        }
    ]

    create_publicwall = {
        "wall_no": 1,
        "wall_name": "作品分享",
        "wall_desc": "",
        "wall_files": [
            {
                "file_no": 1,
                "file_name": "wall_1.jpg",
                "file_content_type": "image/jpeg",
                "image_url": "(http://...)"
            }
        ]
    }

    homework_personal_list = [
        {
            "homeworkpersonal_no": 1,
            "man_no": 1,
            "files": [
                {
                    "file_no": 1,
                    "file_name": "(file name)",
                    "file_size": 123,
                    "file_content_type": "application/octet-stream",
                    "file_download_url": "(http://...)"
                }
            ]
        },
        {
            "homeworkpersonal_no": 2,
            "man_no": 1,
            "files": [
                {
                    "file_no": 2,
                    "file_name": "(file name)",
                    "file_size": 123,
                    "file_content_type": "application/octet-stream",
                    "file_download_url": "(http://...)"
                }
            ]
        }
    ]

    create_homework_personal = {
        "homeworkpersonal_no": 1,
        "man_no": 1
    }

    update_project = {}

    get_chat_groups = [
        {
            'group_name': '施堯仁'
            'message': '456'
            'time': '2017/4/24 09:14'
            'group_type': 1
            'chat_guid': 'd92e9b04-d8a6-4304-8002-42034c67ac5e'
        },
        {
            'group_name': 'Ben lee'
            'message': '信麼東銀字水系人比最好不親次議望實模品，受角展熱步受設標算一服花知資他個在道：致營新文會醫不統。'
            'time': '2017/4/24 09:14'
            'group_type': 1
            'chat_guid': 'fd4a218a-318f-4243-a4c1-9e6619a82278'
        }
    ]

    get_chat_message_history = [
        {
            'man_no': 5
            'man_name': '张琮华'
            'message': 'test'
            'createdate': '2017/4/18 14:01:05'
        },
        {
            'man_no': 2
            'man_name': 'chclee'
            'message': '信麼東銀字水系人比最好不親次議望實模品，受角展熱步受設標算一服花知資他個在道：致營新文會醫不統。'
            'createdate': '2017/4/18 14:03:15'
        },
        {
            'man_no': 5
            'man_name': '张琮华'
            'message': '朋每有裡了輕麼服客和市精不，師行讀單些孩陽！父愛孩的，精士富電聯在因河車求相，想不學。怎強頭見小，作我著利？'
            'createdate': '2017/4/18 14:01:05'
        }
    ]

    get_room_list = [
        {
            'room_no': 1
            'room_name': '施堯仁'
            'room_subname': 'yjshih@sce.pccu.edu.tw'
            'chat_guid': 'd92e9b04-d8a6-4304-8002-42034c67ac5e'
            'room_type': 'personal'
        },
        {
            'room_no': 2
            'room_name': '张琮华'
            'room_subname': 'thchang@sce.pccu.edu.tw'
            'chat_guid': null
            'room_type': 'personal'
        },
        {
            'room_no': 3
            'room_name': 'Ben lee'
            'room_subname': 'chclee@sce.pccu.edu.tw'
            'chat_guid': 'fd4a218a-318f-4243-a4c1-9e6619a82278'
            'room_type': 'personal'
        },
        {
            'room_no': 2
            'room_name': '班級1'
            'room_subname': ''
            'chat_guid': null
            'room_type': 'class'
        }
    ]

    create_chatgroup = {
        'group_name': 'xxx',
        'group_avatar': '',
        'group_type': 1,
        'chat_guid': 'b4c1ddd2-507a-41ba-9d55-6963073ad52d',
        'message': '',
        'time': ''
    }

    homework_request_list_teacher = [
        {
            'homework_request_no': 1
            'project_no': 1
            'homework_request_name': '雕塑作業'
            'homework_request_desc': '人像陶土雕塑練習'
            'homework_request_type': 'Personal'
            'homework_request_deadline': '2017-07-16T14:02:27'
            'homeworks': null
        }
        {
            'homework_request_no': 2
            'project_no': 1
            'homework_request_name': '雕塑作業2'
            'homework_request_desc': '物件陶土雕塑練習'
            'homework_request_type': 'Team'
            'homework_request_deadline': '2017-08-16T14:02:27'
            'homeworks': null
        }
    ]

    my_homework_request_list = [
        {
            'homework_request_no': 1
            'project_no': 1
            'homework_request_name': '雕塑作業'
            'homework_request_desc': '人像陶土雕塑練習'
            'homework_request_type': 'Personal'
            'homework_request_deadline': '2017-07-16T14:02:27'
            'homeworks': [
                {
                    'homework_no': 1
                    'homework_request_no': 1
                    'homework_request_name': '雕塑作業1'
                    'homework_request_type': 'Personal'
                    'team_no': 1
                    'score': 95
                    'files': [
                        {
                            'file_no': 1
                            'file_name': '陶土作業1.jpg'
                            'file_size': 123
                            'file_content_type': 'image/jpeg'
                            'file_url': 'http://10.168.18.154/v1/phaedoapi/file/678c040d489842a8852680f880a7f299'
                            'file_creator_name': '王明'
                        }
                    ]
                }
            ]
        }
        {
            'homework_request_no': 2
            'project_no': 1
            'homework_request_name': '雕塑作業2'
            'homework_request_desc': '物件陶土雕塑練習'
            'homework_request_type': 'Team'
            'homework_request_deadline': '2017-08-16T14:02:27'
            'homeworks': [
                {
                    'homework_no': 2
                    'homework_request_no': 2
                    'homework_request_name': '雕塑作業2'
                    'homework_request_type': 'Personal'
                    'team_no': null
                    'score': null
                    'files': [
                        {
                            'file_no': 2
                            'file_name': '陶土作業2.mp3'
                            'file_size': 123
                            'file_content_type': 'mp3'
                            'file_url': 'https://drive.google.com/file/d/0B4zazagi1f1kM3pWZnFxUmFpR3c/view?usp=sharing'
                            'file_creator_name': '許凱'
                        }
                    ]
                }
            ]
        }
        {
            'homework_request_no': 3
            'project_no': 1
            'homework_request_name': '雕塑作業3'
            'homework_request_desc': '物件陶土雕塑練習2'
            'homework_request_type': 'Team'
            'homework_request_deadline': '2017-08-16T14:02:27'
            'homeworks': null
        }
        {
            'homework_request_no': 4
            'project_no': 1
            'homework_request_name': '雕塑作業4'
            'homework_request_desc': '物件陶土雕塑練習3'
            'homework_request_type': 'Team'
            'homework_request_deadline': '2017-08-26T14:02:27'
            'homeworks': null
        }
    ]

    homework_list = [
        {
            'homework_no': 1
            'homework_request_no': 1
            'group_no': 1
            'score': 95
            'files': [
                {
                    'file_no': 1
                    'file_name': '(file name)'
                    'file_size': 123
                    'file_content_type': 'application/octet-stream'
                    'file_download_url': '(http://...)'
                }
            ]
        }
        {
            'homework_no': 2
            'homework_request_no': 2
            'group_no': 1
            'score': null
            'files': [
                {
                    'file_no': 2
                    'file_name': '(file name)'
                    'file_size': 123
                    'file_content_type': 'application/octet-stream'
                    'file_download_url': '(http://...)'
                }
            ]
        }
        {
            'homework_no': 3
            'homework_request_no': null
            'group_no': null
            'score': null
            'files': [
                {
                    'file_no': 3
                    'file_name': '(file name)'
                    'file_size': 123
                    'file_content_type': 'application/octet-stream'
                    'file_download_url': '(http://...)'
                }
            ]
        }
    ]

    homework = {

    }

    homework_list_for_stuent = [
        {
            'homework_no': 1
            'homework_request_no': 1
            'homework_request_name': '雕塑作業1'
            'homework_request_type': 'Personal'
            'team_no': 1
            'score': 95
            'files': [
                {
                    'file_no': 1
                    'file_name': '陶土作業1.jpg'
                    'file_size': 123
                    'file_content_type': 'image/jpeg'
                    'file_url': 'http://10.168.18.154/v1/phaedoapi/file/678c040d489842a8852680f880a7f299'
                    'file_creator_name': '王明'
                }
            ]
        }
        {
            'homework_no': 2
            'homework_request_no': 2
            'homework_request_name': '雕塑作業2'
            'homework_request_type': 'Personal'
            'team_no': null
            'score': null
            'files': [
                {
                    'file_no': 2
                    'file_name': '陶土作業2.mp3'
                    'file_size': 123
                    'file_content_type': 'mp3'
                    'file_url': 'https://drive.google.com/file/d/0B4zazagi1f1kM3pWZnFxUmFpR3c/view?usp=sharing'
                    'file_creator_name': '許凱'
                }
            ]
        }
        {
            'homework_no': 3
            'homework_request_no': null
            'homework_request_name': null
            'homework_request_type': null
            'team_no': null
            'score': null
            'files': [
                {
                    'file_no': 3
                    'file_name': '陶土作業3.jpg'
                    'file_size': 123
                    'file_content_type': 'image/jpeg'
                    'file_url': 'http://10.168.18.154/v1/phaedoapi/file/81ac1b0d29f44823bd43623d7c343615'
                    'file_creator_name': '陳雪'
                }
            ]
        }
        {
            'homework_no': 4
            'homework_request_no': null
            'homework_request_name': null
            'homework_request_type': null
            'team_no': null
            'score': null
            'files': [
                {
                    'file_no': 3
                    'file_name': '陶土作業4.jpg'
                    'file_size': 123
                    'file_content_type': 'image/jpeg'
                    'file_url': 'http://10.168.18.154/v1/phaedoapi/file/81ac1b0d29f44823bd43623d7c343615'
                    'file_creator_name': '陳雪'
                }
            ]
        }
    ]

    return {
        login: login
        course_list: course_list
        manscore: manscore
        manscore_list: manscore_list
        project_list: project_list
        create_project: create_project
        project: project
        project_score: project_score
        group_list: group_list
        evaluate_list: evaluate_list
        create_evaluate: create_evaluate
        create_group: create_group
        create_groupman: create_groupman
        create_homework: create_homework
        homework_list: homework_list
        publicwall_list: publicwall_list
        airmap_list: airmap_list
        create_publicwall: create_publicwall
        homework_personal_list: homework_personal_list
        create_homework_personal: create_homework_personal
        update_project: update_project
        get_chat_groups: get_chat_groups
        get_chat_message_history: get_chat_message_history
        get_room_list: get_room_list
        create_chatgroup: create_chatgroup
        
        my_homework_request_list: my_homework_request_list
        homework_list_for_stuent: homework_list_for_stuent
    }