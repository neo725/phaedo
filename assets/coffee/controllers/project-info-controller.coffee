constants = require('../common/constants')

module.exports = ['$rootScope', '$scope', '$sce', '$log', '$timeout', '$uibModal',
    'CourseService', 'ProjectService', 'ManscoreService', 'UserService', 'msgbox', 'api',
    ($rootScope, $scope, $sce, $log, $timeout, $uibModal,
        CourseService, ProjectService, ManscoreService, UserService, msgbox, api) ->
            $scope.group_list = []
            $scope.group = {}
            $scope.evaluate_list = []
            $scope.evaluate = {}

            course = CourseService.get()
            project = ProjectService.get()
            manscore = ManscoreService.get(course.course_no)
            user = UserService.get()

            $scope.course = course
            $scope.manscore = manscore

            $scope.intro_editable = false
            $scope.prestudy_editable = false
            $scope.material_editable = false

            $scope.parseHTML = (str) ->
                if str
                    str = str.replace(/(?:\r\n|\r|\n)/g, '<br />')
                str = $sce.trustAsHtml(str)
                return str

            $scope.openCreateNewGroup = ->
                new_group = {
                    name: '分組'
                }
                modalInstance = $uibModal.open(
                    animation: true
                    size: 'lg'
                    #backdrop: 'static'
                    templateUrl: 'modalGroupInfo.html'
                    controller: 'ModalGroupInfoController'
                    resolve:
                        new_group: ->
                            return new_group
                        group_list: ->
                            return $scope.group_list
                        course: ->
                            return course
                        project: ->
                            return project
                )
                modalInstance.result.then (group_list) ->
                    $scope.group_list = group_list

            $scope.checkUserExistsInGroup = (user, group) ->
                index = _.findIndex(group.group_man, { man_no: user.man_no })
                return index != -1

            $scope.openGroupModal = (group) ->
                $uibModal.open(
                    animation: true
                    size: 'lg'
                    #backdrop: 'static'
                    windowClass: 'modal-project-info modal-project-group-homework'
                    templateUrl: 'modalGroup.html'
                    controller: 'ModalProjectGroupController'
                    resolve:
                        course: ->
                            return course
                        project: ->
                            return project
                        user: ->
                            return user
                        group: ->
                            return group
                        manscore: ->
                            return $scope.manscore
                )

            $scope.deleteGroup = (group) ->
                onYes = () ->
                    course_no = course.course_no
                    project_no = project.project_no
                    group_no = group.group_no

                    onSuccess = (response) ->
                        $log.info response
                        if response == true
                            index = _.findIndex($scope.group_list, { group_no: group.group_no })
                            if index > -1
                                $scope.group_list.splice(index, 1)
                    onError = (->)

                    api.deleteGroup(course_no, project_no, group_no, onSuccess, onError)
                onNo = (->)

                msgbox.confirm 'message.confirm_sure_to_delete_project_team', onYes, onNo

            $scope.openChat = (man, $event) ->
                if man.man_no == user.man_no
                    return false

                if $scope.manscore.manscore_type == 2000
                    return $scope.openHomeworkModal(man, $event)

                onSuccess = (response) ->
                    data =
                        room_type: 'personal'
                        data_no: man.man_no
                        chat_guid: response.chat_guid

                    $rootScope.$broadcast('pd:visible:chatmodal_show', data)

                onError = (->)

                data = {
                    receiver: man.man_no
                }

                api.getChatGroupsFromChat data, onSuccess, onError

            $scope.openHomeworkModal = (man, $event) ->
                $uibModal.open(
                    animation: true
                    size: 'lg'
                    #backdrop: 'static'
                    windowClass: 'modal-project-info modal-project-homework'
                    templateUrl: 'modalHomework.html'
                    controller: 'ModalHomeworkController'
                    resolve:
                        course: ->
                            return course
                        project: ->
                            return project
                        user: ->
                            return user
                        man: ->
                            return man
                        manscore: ->
                            return $scope.manscore
                )
                #$scope.loadEvaluateList(man, $event)

            $scope.openMyHomeworkModal = () ->
                $uibModal.open(
                    animation: true
                    size: 'lg'
                    #backdrop: 'static'
                    windowClass: 'modal-project-info modal-project-homework'
                    templateUrl: 'modalMyHomework.html'
                    controller: 'ModalMyHomeworkController'
                    resolve:
                        project: ->
                            return project
                        user: ->
                            return user
                )

            $scope.addMyNote = () ->
                modalInstance = $uibModal.open(
                    animation: true
                    size: 'lg'
                    #backdrop: 'static'
                    #windowClass: 'modal-project-info modal-project-homework'
                    templateUrl: 'modalMyNote.html'
                    controller: 'ModalMyNoteController'
                    resolve:
                        project: ->
                            return project
                        note: ->
                            return null
                )

                modalInstance.result.then (note) ->
                    $scope.notes.push note

            $scope.editMyNote = (index, note) ->
                modalInstance = $uibModal.open(
                    animation: true
                    size: 'lg'
                    #backdrop: 'static'
                    #windowClass: 'modal-project-info modal-project-homework'
                    templateUrl: 'modalMyNote.html'
                    controller: 'ModalMyNoteController'
                    resolve:
                        project: ->
                            return project
                        note: ->
                            return note
                )

                modalInstance.result.then (note) ->
                    $scope.notes[index] = note

            $scope.deleteMyNote = (index, note) ->          
                onYes = ->
                    onSuccess = (response) ->
                        $timeout ->
                            $scope.notes.splice index, 1
                        msgbox.success 'message.data_delete_successed'
                    onError = ->
                        msgbox.error 'errors.data_delete_failure'
                    api.deleteNote(note.note_uid, onSuccess, onError)
                onNo = (->)

                msgbox.confirm 'message.confirm_sure_to_delete_note', onYes, onNo

            $scope.loadEvaluateList = (man, $event) ->
                $event.stopPropagation()

                $scope.openChat(man, $event)

                if (manscore.manscore_type != 2000)
                    return

                $scope.target_man = man
                loadEvaluateList(man)

            $scope.createEvaluate = (evaluate) ->
                evaluate.course_no = course.course_no
                evaluate.project_no = project.project_no
                evaluate.man_no = $scope.target_man.man_no

                onSuccess = () ->
                    $scope.evaluate_list.push evaluate.evaluate_info
                    evaluate.evaluate_info = ''
                onError = (->)

                api.createEvaluate evaluate, onSuccess, onError

            $scope.closeEvaluateList = ->
                delete $scope['target_man']

            $scope.showEdit = (project_info, variable_name, target_show) ->
                if manscore.manscore_type != 2000
                    return

                $scope[target_show] = true

                if variable_name == 'project_intro'
                    project_info.project_intro_copy = angular.copy(project_info.project_intro)

                if variable_name == 'project_prestudy'
                    project_info.project_prestudy_copy = angular.copy(project_info.project_prestudy)

                if variable_name == 'project_material'
                    project_info.project_material_copy = angular.copy(project_info.project_material)

                return

            $scope.hideEdit = (target_hide) ->
                $scope[target_hide] = false

                return

            $scope.edit = (project_info, variable_name, target_hide) ->
                if manscore.manscore_type != 2000
                    return

                column_value = angular.copy(project_info[variable_name + '_'])

                if variable_name == 'project_intro'
                    column_value = angular.copy(project_info.project_intro_copy)

                if variable_name == 'project_prestudy'
                    column_value = angular.copy(project_info.project_prestudy_copy)

                if variable_name == 'project_material'
                    column_value = angular.copy(project_info.project_material_copy)

                onSuccess = () ->
                    $scope[target_hide] = false

                    if variable_name == 'project_intro'
                        project_info.project_intro = angular.copy(project_info.project_intro_copy)

                    if variable_name == 'project_prestudy'
                        project_info.project_prestudy = angular.copy(project_info.project_prestudy_copy)

                    if variable_name == 'project_material'
                        project_info.project_material = angular.copy(project_info.project_material_copy)

                onError = (->)

                api.updateProject(course.course_no, project_info.project_no, variable_name, column_value, onSuccess, onError)

            loadEvaluateList = (man) ->
                onSuccess = (response) ->
                    #$scope.evaluate_list = angular.copy response
                    $scope.evaluate_list = response
                onError = (->)

                api.getProjectEvaluateList course.course_no, project.project_no, man.man_no, onSuccess, onError

            loadManList = () ->
                onSuccess = (response) ->
#                    if manscore.manscore_type == 2000
#                        return $scope.man_list = response
#                    man = _.find(response, { man_no: manscore.man_no })
#                    $scope.man_list = [man]
                    me = null
                    mans = _.filter(response, (user) ->
                        if user.man_no == manscore.man_no
                            me = user
                        return user.man_no != manscore.man_no
                    )
                    if me
                        mans.unshift me
                    $scope.man_list = mans
                onError = (->)

                api.getManscoreList(course.course_no, project.project_no, onSuccess, onError)

            loadGroupList = ->
                onSuccess = (response) ->
#                    if manscore.manscore_type == 2000
#                        return $scope.group_list = response
#                    group_list = _.filter(response, (group) ->
#                        groupman = _.find(group.group_man, { man_no: manscore.man_no })
#                        return groupman != undefined
#                    )
#                    $scope.group_list = group_list
                    $scope.group_list = response

                onError = (->)

                api.getGroupList course.course_no, project.project_no, onSuccess, onError

            loadProjectScore = ->
                if manscore.manscore_type == 2000
                    return

                onSuccess = (response) ->
                    $scope.project_score = response
                onError = (->)

                api.getProjectScore course.course_no, project.project_no, manscore.man_no, onSuccess, onError

            loadProjectInfo = ->
                onSuccess = (response) ->
                    $scope.project_info = response
                    $scope.teacher_list = [response.teacher_info]
                onError = (->)

                api.getProject course.course_no, project.project_no, onSuccess, onError

            loadProjectChatGroup = ->
                onSuccess = (response) ->
                    $scope.project_chat_guid = response.chat_guid
                onError = (->)

                data =
                    project_no: project.project_no
                api.getChatGroupsFromChat data, onSuccess, onError

            loadNotes = () ->
                onSuccess = (response) ->
                    $scope.notes = response.note_list
                onError = (->)

                api.getNotes project.project_no, onSuccess, onError

            if course == null
                return

            loadProjectInfo()
            loadProjectScore()
            loadManList()
            loadGroupList()
            loadProjectChatGroup()
            loadNotes()
]