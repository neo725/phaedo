constants = require('../common/constants')

module.exports = ['$location', '$q', '$translate', 'UserService', 'msgbox',
    ($location, $q, $translate, UserService, msgbox) ->
        canceller = $q.defer()
        response: (response) ->
            response || $q.when response

        responseError: (rejection) ->
            config = rejection.config

            isHeartBeat = /\/phaedoapi\/heartbeat$/.test(config.url)

            if rejection.status == 401
                UserService.clear()
                canceller.resolve('Token not found.')
                $location.url '/login'
                return $q.reject rejection

            if rejection.status == 403
                UserService.clear()
                canceller.resolve('Forbidden')
                $location.url '/login'
                return $q.reject rejection

#            if isHeartBeat and rejection.status == 403
#                UserService.setIsValid(false)
#                canceller.resolve('Forbidden')
#                $location.url '/login'
#                return $q.reject rejection

            if not isHeartBeat and rejection.data and rejection.data['error_message']
                isvalid = UserService.getIsValid()
                if isvalid
                    msgbox.error rejection.data['error_message']

            $q.reject rejection

        request: (config) ->
            #config.timeout = 5000
            canceller = $q.defer()
            api_endpoint = "#{constants.API_URL}"

            isApiRequest = /^\/phaedoapi\//.test(config.url)
            isHeartBeat = /\/phaedoapi\/heartbeat$/.test(config.url)

            if isApiRequest
                config.url = "#{api_endpoint}#{config.url}"
                config.timeout = canceller.promise

                user = UserService.get()
                if user
                    token = user.token
                    config.headers['token'] = token
#                    if isHeartBeat
#                        config.headers['token'] = '12345'

                config || $q.when config

            return config
]