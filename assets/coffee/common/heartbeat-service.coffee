module.exports = ['$http', '$timeout', '$log', 'UserService', 'api', 'HeartbeatHubService',
    ($http, $timeout, $log, UserService, api, HeartbeatHubService) ->

        interval = 5 * 60 * 1000
        #interval = 20 * 1000

        keepHeartBeat = () ->
            user = UserService.get()
#            onSuccess = (response) ->
#                #console.log response
#                $timeout keepHeartBeat, 5 * 60 * 1000
#            onError = (error, status_code) ->
#                console.log error
#                console.log status_code
#                detectHeartBeatReady()
#
#            api.sendHeartBeat onSuccess, onError
            HeartbeatHubService.keepAlive user.token

            # every 5 minutes
            $timeout keepHeartBeat, interval

        detectHeartBeatReady = () ->
            state = HeartbeatHubService.getConnectState()
            if state == 'disconnected'
                $timeout detectHeartBeatReady, 1000
                return

            user = UserService.get()
            isvalid = UserService.getIsValid()

            if not user
                $timeout detectHeartBeatReady, 1000
                return

            if not isvalid
                $timeout detectHeartBeatReady, 1000
                return

            keepHeartBeat()

        detectHeartBeatReady()
]