module.exports = ['$rootScope', '$scope', '$log',
    ($rootScope, $scope, $log) ->

        $scope.toggleCalendar = ->
            $rootScope.$broadcast('pd:visible:calendar_show')
]