module.exports = ['$http', 'FakeJsonService', ($http, FakeJsonService) ->

    readJson = (json_key) ->
        json = FakeJsonService[json_key]
        return json

    sendPost = (url, data, onSuccess, onError, fake_json, is_fake) ->
        $http.post(url, data)
        .success((response) ->
            if is_fake and fake_json
                return onSuccess(readJson(fake_json))
            return onSuccess(response)
        )
        #.error(onError)
        .error((error, status_code) ->
            if is_fake and fake_json
                return onSuccess(readJson(fake_json))
            return onError(error, status_code)
        )

    sendPut = (url, data, onSuccess, onError, fake_json, is_fake) ->
        $http.put(url, data)
        .success((response) ->
            if is_fake and fake_json
                return onSuccess(readJson(fake_json))
            return onSuccess(response)
        )
        #.error(onError)
        .error((error, status_code) ->
            if is_fake and fake_json
                return onSuccess(readJson(fake_json))
            return onError(error, status_code)
        )

    sendGet = (url, onSuccess, onError, fake_json, is_fake) ->
        $http.get(url)
        .success((response) ->
            if is_fake and fake_json
                return onSuccess(readJson(fake_json))
            return onSuccess(response)
        )
        #.error(onError)
        .error((error, status_code) ->
            if is_fake and fake_json
                return onSuccess(readJson(fake_json))
            return onError(error, status_code)
        )

    sendDelete = (url, onSuccess, onError, fake_json, is_fake) ->
        $http.delete(url)
        .success((response) ->
            if is_fake and fake_json
                return onSuccess(readJson(fake_json))
            return onSuccess(response)
        )
        .error((error, status_code) ->
            if is_fake and fake_json
                return onSuccess(readJson(fake_json))
            return onError(error, status_code)
        )

    api = {
        login: (user, onSuccess, onError) ->
            sendPost('/phaedoapi/man/login', user, onSuccess, onError, 'login', false)

        getCourseList: (archive, onSuccess, onError) ->
            sendGet("/phaedoapi/course/list?archive=#{archive}", onSuccess, onError, 'course_list', false)

        getCourseListByType: (onSuccess, onError) ->
            sendGet('/phaedoapi/course/ListByType', onSuccess, onError)

        getProjectList: (course_no, onSuccess, onError) ->
            sendGet("/phaedoapi/project/list?course_no=#{course_no}", onSuccess, onError, 'project_list', false)

        getManscore: (course_no, onSuccess, onError) ->
            sendGet("/phaedoapi/manscore?course_no=#{course_no}", onSuccess, onError, 'manscore', false)

        createProject: (project, onSuccess, onError) ->
            sendPost('/phaedoapi/project', project, onSuccess, onError, 'create_project', false)

        getProject: (course_no, project_no, onSuccess, onError) ->
            sendGet("/phaedoapi/project/info?course_no=#{course_no}&project_no=#{project_no}",
                onSuccess, onError, 'project', false)

        getProjectScore: (course_no, project_no, man_no, onSuccess, onError) ->
            sendGet("/phaedoapi/project/score?course_no=#{course_no}&project_no=#{project_no}&man_no=#{man_no}",
                onSuccess, onError, 'project_score', false)

        getProjectEvaluateList: (course_no, project_no, man_no, onSuccess, onError) ->
            sendGet("/phaedoapi/evaluate/list?course_no=#{course_no}&project_no=#{project_no}&man_no=#{man_no}",
                onSuccess, onError, 'evaluate_list', false)

        getGroupList: (course_no, project_no, onSuccess, onError) ->
            sendGet("/phaedoapi/group/list?course_no=#{course_no}&project_no=#{project_no}",
                onSuccess, onError, 'group_list', false)

        createEvaluate: (evaluate, onSuccess, onError) ->
            sendPost('/phaedoapi/evaluate', evaluate, onSuccess, onError, 'create_evaluate', false)

        getManscoreList: (course_no, project_no, onSuccess, onError) ->
            sendGet("/phaedoapi/manscore/list?course_no=#{course_no}&project_no=#{project_no}",
                onSuccess, onError, 'manscore_list', false)

        createGroup: (group, onSuccess, onError) ->
            sendPost('/phaedoapi/group', group, onSuccess, onError, 'create_group', false)

        createGroupMan: (group_man_list, onSuccess, onError) ->
            sendPost('/phaedoapi/groupman', group_man_list, onSuccess, onError, 'create_groupman', false)

        createHomework: (homework, onSuccess, onError) ->
            sendPost('/phaedoapi/homework', homework, onSuccess, onError, 'create_homework', false)

        createHomeworkPersonal: (homework, onSuccess, onError) ->
            sendPost('/phaedoapi/homework/personal', homework, onSuccess, onError, 'create_homework_personal', false)

        getHomeworkList: (group_no, onSuccess, onError) ->
            sendGet("/phaedoapi/homework/list?group_no=#{group_no}", onSuccess, onError, 'homework_list', false)

        getPublicWallList: (page, pagesize, onSuccess, onError) ->
            sendGet("/phaedoapi/sharedwall/list?page=#{page}&pagesize=#{pagesize}", onSuccess, onError,
                'publicwall_list', false)

        getAirMapList: (onSuccess, onError) ->
            sendGet("/phaedoapi/airmap/list", onSuccess, onError, 'airmap_list', false)

        createPublicwall: (wall, onSuccess, onError) ->
            sendPost('/phaedoapi/publicwall', wall, onSuccess, onError, 'create_publicwall', false)

        getHomeworkPersonalList: (man_no, onSuccess, onError) ->
            sendGet("/phaedoapi/homework/personal?man_no=#{man_no}", onSuccess, onError, 'homework_personal_list', false)

        updateProject: (course_no, project_no, column_name, column_value, onSuccess, onError) ->
            data =
                course_no: course_no
                project_no: project_no
                column_name: column_name
                column_value: column_value
            sendPut('/phaedoapi/project/update', data, onSuccess, onError, 'update_project', false)

        sendHeartBeat: (onSuccess, onError) ->
            sendPut('/phaedoapi/heartbeat', {}, onSuccess, onError)

        sendWXLogin: (onSuccess, onError) ->
            sendGet('/phaedoapi/man/WXLogin', onSuccess, onError)

        validateWXLogin: (state, onSuccess, onError) ->
            sendGet("/phaedoapi/ValidateWXLogin?state=#{state}", onSuccess, onError)

        getChatGroups: (onSuccess, onError) ->
            sendGet("/phaedoapi/push/chatgroups", onSuccess, onError, 'get_chat_groups', false)

        getChatMessageHistory: (chat_guid, onSuccess, onError) ->
            sendGet("/phaedoapi/push/messagehistory?chat_guid=#{chat_guid}", onSuccess, onError, 'get_chat_message_history', false)

        getRoomList: (onSuccess, onError) ->
            sendGet("/phaedoapi/push/roomlist", onSuccess, onError, 'get_room_list', false)

        createChatGroups: (data, onSuccess, onError) ->
            sendPost('/phaedoapi/push/chatgroups', data, onSuccess, onError, 'create_chatgroup', false)

        markChatMessageAsReaded: (list, onSuccess, onError) ->
            sendPut('/phaedoapi/push/messageread', list, onSuccess, onError)

        getUsersInChatroom: (chat_guid, onSuccess, onError) ->
            sendGet("/phaedoapi/push/members?chat_guid=#{chat_guid}", onSuccess, onError)

        getPushChatGroup: (onSuccess, onError) ->
            sendGet('/phaedoapi/push/pushchatgroup', onSuccess, onError)

        getExhibitions: (onSuccess, onError) ->
            sendGet('/phaedoapi/exhibition/list', onSuccess, onError)

        getExhibitionItems: (exhibition_no, onSuccess, onError) ->
            sendGet("/phaedoapi/exhibition/#{exhibition_no}", onSuccess, onError)

        updateExhibition: (exhibition_no, data, onSuccess, onError) ->
            sendPut("/phaedoapi/exhibition/#{exhibition_no}", data, onSuccess, onError)

        createExhibition: (data, onSuccess, onError) ->
            sendPost('/phaedoapi/exhibition', data, onSuccess, onError)

        deleteExhibition: (exhibition_no, onSuccess, onError) ->
            sendDelete("/phaedoapi/exhibition/#{exhibition_no}", onSuccess, onError)

        updateExhibitionItem: (exhibition_item_no, data, onSuccess, onError) ->
            sendPut("/phaedoapi/exhibitionItem/#{exhibition_item_no}", data, onSuccess, onError)

        deleteExhibitionItem: (exhibition_item_no, onSuccess, onError) ->
            sendDelete("/phaedoapi/exhibitionItem/#{exhibition_item_no}", onSuccess, onError)

        deleteExhibitionItemAttachment: (attachment_no, onSuccess, onError) ->
            sendDelete("/phaedoapi/exhibitionItem/attachment/#{attachment_no}", onSuccess, onError)

        getChatGroupsFromChat: (data, onSuccess, onError) ->
            sendPost('/phaedoapi/chat/chatgroups', data, onSuccess, onError)

        getChatMessageHistoryFromChat: (chat_guid, onSuccess, onError) ->
            sendGet("/phaedoapi/chat/messagehistory?chat_guid=#{chat_guid}", onSuccess, onError)

        postLike: (type, data_id, onSuccess, onError) ->
            sendPost("/phaedoapi/like/#{type}/#{data_id}", {}, onSuccess, onError)

        ###########
        # getHomeworkRequests: (project_no, onSuccess, onError) ->
        #     sendGet('/phaedoapi/exhibition/list', onSuccess, onError, 'my_homework_request_list', true)

        # getHomeworkRequests: (project_no, onSuccess, onError) ->
        #     sendGet("/phaedoapi/ProjectWork/Request/Student/List/#{project_no}", onSuccess, onError)

        getHomeworkRequests: (project_no, onSuccess, onError) ->
            sendGet("/phaedoapi/ProjectWork/Response/Student/List/#{project_no}", onSuccess, onError)

        # getHomeworks: (onSuccess, onError) ->
        #     sendGet('/phaedoapi/exhibition/list', onSuccess, onError, 'homework_list_for_stuent', true)

        # getHomeworks: (project_no, onSuccess, onError) ->
        #     sendGet("/phaedoapi/ProjectWork/Student/List/#{project_no}", onSuccess, onError)

        getHomeworks: (project_no, onSuccess, onError) ->
            sendGet("/phaedoapi/ProjectWork/StudentFiles/List/#{project_no}", onSuccess, onError)

        getNotes: (project_no, onSuccess, onError) ->
            sendGet("/phaedoapi/Note/List/#{project_no}", onSuccess, onError)

        createNote: (project_no, data, onSuccess, onError) ->
            sendPost("/phaedoapi/Note/#{project_no}", data, onSuccess, onError)

        updateNote: (note_uid, data, onSuccess, onError) ->
            sendPut("/phaedoapi/Note/#{note_uid}", data, onSuccess, onError)

        deleteNote: (note_uid, onSuccess, onError) ->
            sendDelete("/phaedoapi/Note/#{note_uid}", onSuccess, onError)

        uploadFile: (data, onSuccess, onError) ->
            sendPost("/phaedoapi/file/upload", data, onSuccess, onError)

        # uploadFileInProject: (data, onSuccess, onError) ->
        #     sendPost("/phaedoapi/ProjectWork/Create", data, onSuccess, onError)

        uploadFileInProject: (data, onSuccess, onError) ->
            sendPost("/phaedoapi/ProjectWork/UploadWorkFile", data, onSuccess, onError)

        # submitHomework: (homework_request_no, homework_no, onSuccess, onError) ->
        #     sendPut("/phaedoapi/ProjectWork/Submit/#{homework_request_no}/#{homework_no}", {}, onSuccess, onError)

        submitHomework: (homework_request_no, homework_no, onSuccess, onError) ->
            sendPut("/phaedoapi/ProjectWork/SubmitResponse/#{homework_request_no}/#{homework_no}", {}, onSuccess, onError)

        deleteGroup: (course_no, project_no, group_no, onSuccess, onError) ->
            sendDelete("/phaedoapi/ProjectTeam/DeleteProjectTeam/#{course_no}/#{project_no}/#{group_no}", onSuccess, onError)

        createTask: (title, description, deadline, onSuccess, onError) ->
            data =
                title: title
                description: description
                deadline: deadline
            sendPost('/phaedoapi/tasks/create', data, onSuccess, onError)

        getTasks: (onSuccess, onError) ->
            sendGet('/phaedoapi/tasks/getmembertasks', onSuccess, onError)

        updateTask: (taskId, title, description, deadline, onSuccess, onError) ->
            data =
                title: title
                description: description
                deadline: deadline
            sendPut("/phaedoapi/tasks/#{taskId}", data, onSuccess, onError)

        deleteTask: (taskId, onSuccess, onError) ->
            sendDelete("/phaedoapi/tasks/#{taskId}", onSuccess, onError)
    }

    return api
]