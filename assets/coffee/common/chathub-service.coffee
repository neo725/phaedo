constants = require('./constants')

module.exports = ['$rootScope', 'Hub', '$timeout', '$q', '$log', 'UserService',
    ($rootScope, Hub, $timeout, $q, $log, UserService) ->
        _self = {}
        _self.connect_state = 'disconnected'
        _self.server_methods = ['hello', 'joinGroup', 'sendMessage', 'leaveGroups']

        watchUserFunc = ->
            return UserService.getIsLogin()

        $rootScope.$watch(watchUserFunc, (newValue, oldValue) ->
            $log.info 'User watch changed ...'
            if _self.hub and newValue != oldValue
                if newValue
                    user = UserService.get()

                    _self.hub.connection.qs = { 'token': user.token }
                else
                    _self.hub.connection.qs = {}

                _self.hub.disconnect()
        )

        connect = (listeners, state_listeners) ->
            user = UserService.get()
            if user
                token = user.token

            if _self.hub
                if token
                    _self.hub.connection.qs = { 'token': token }
                _self.hub.connect()
                return

            _self.listeners = {}
            for propertyName of listeners
                _self.listeners[propertyName] = do (propertyName) ->
                    ->
                        listeners[propertyName].apply listeners[propertyName], arguments
                        #$rootScope.$apply()
                        return

            _self.hub = new Hub('chatHub', {
                # client side method
                listeners: _self.listeners

                # server side method
                methods: _self.server_methods

                # query params sent on initial connection
                queryParams: { 'token': token }

                # handle connection error
                errorHandler: (error) ->
                    $log.error error

                # specify a non default root
                rootPath: constants.SIGNALR_URL

                stateChanged: (state) ->
                    switch state.newState
                        when $.signalR.connectionState.connecting
                            _self.connect_state = 'connecting'
                            console.log 'signalr state : connecting...'
                            $timeout ->
                                state_listeners.connecting()
                                $rootScope.$apply()
                                return
                        when $.signalR.connectionState.connected
                            _self.connect_state = 'connected'
                            console.log 'signalr state : connected'
                            $timeout ->
                                state_listeners.connected()
                                $rootScope.$apply()
                                return
                        when $.signalR.connectionState.reconnecting
                            _self.connect_state = 'reconnecting'
                            console.log 'signalr state : reconnecting...'
                            $timeout ->
                                state_listeners.reconnecting()
                                $rootScope.$apply()
                                return
                        when $.signalR.connectionState.disconnected
                            _self.connect_state = 'disconnected'
                            console.log 'signalr state : disconnected'
                            $timeout ->
                                state_listeners.disconnected()
                                $rootScope.$apply()
                                return
            })

            return _self.hub

        disconnect = () ->
            if _self.hub
                _self.hub.disconnect()

        bind = (scope, name, fn) ->
            whenHubReady = () ->
                item = {
                    scope: scope
                    name: name
                }
                if _self.hub
                    _self.hub.on(name, fn)
                    return
                $timeout whenHubReady
            whenHubReady()

        hello = () ->
            _self.hub.hello()

        joinGroup = (chat_guid) ->
            _self.hub.joinGroup(chat_guid)

        sendMessage = (message, chat_guid) ->
            _self.hub.sendMessage(message, chat_guid)

        leaveGroups = () ->
            _self.hub.leaveGroups()

        getConnectState = () ->
            return _self.connect_state

        return {
            'getConnectState': getConnectState
            'connect': connect
            'disconnect': disconnect
            'bind': bind

            'hello': hello
            'joinGroup': joinGroup
            'sendMessage': sendMessage
            'leaveGroups': leaveGroups
        }
]