constants = require('../common/constants')

module.exports = ['$scope', '$log', '$uibModalInstance', 'Upload', 'UserService', 'msgbox', 'api', 'exhibition',
    ($scope, $log, $uibModalInstance, Upload, UserService, msgbox, api, exhibition) ->
        attachmentTypeName = 'exhibitionBanner'

        $scope.dateOptions =
            maxDate: new Date(2020, 5, 22)
            minDate: new Date()
            startingDay: 1

        $scope.popupDate =
            opened_1: false
            opened_2: false

        $scope.data =
            start_date: new Date()
            end_date: new Date()

        uploadFiles = (url, data, file, onSuccess, onError) ->
            user = UserService.get()

            if file
                headers = {}
                if user
                    headers.token = user.token

                file.upload = Upload.upload(
                    url: url
                    data: data
                    file: file
                    headers: headers
                )

                file.upload.then (response) ->
                    onSuccess(response)
                , (response) ->
                    if response.status > 0
                        onError(response)
                , (evt) ->
                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total))
        formatDate = (date) ->
            month = '' + (date.getMonth() + 1)
            day = '' + date.getDate()
            year = date.getFullYear()

            '0' + month if month.length < 2
            '0' + day if day.length < 2

            return [year, month, day].join('-')

        newExhibition = (data, success_fn, error_fn) ->
            exhibition =
                exhibition_title: data.title
                exhibition_banner: data.attachment_no
                period_begin: formatDate(data.start_date)
                period_end: formatDate(data.end_date)
                exhibition_mark: 10

            onSuccess = (response) ->
                success_fn 'message.data_create_successed', response
            onError = ->
                error_fn 'errors.data_create_failure'

            api.createExhibition exhibition, onSuccess, onError

        editExhibition = (data, success_fn, error_fn) ->
            exhibition =
                exhibition_title: data.title
                exhibition_banner: data.attachment_no
                period_begin: formatDate(data.start_date)
                period_end: formatDate(data.end_date)
            exhibition_no = $scope.exhibition.exhibition_no

            onSuccess = (response) ->
                success_fn 'message.data_update_successed', response
            onError = ->
                error_fn 'errors.data_update_failure'

            api.updateExhibition(exhibition_no, exhibition, onSuccess, onError)

        $scope.openPopupDate = (type) ->
            $scope.popupDate[type] = true

        $scope.submitForm = (form) ->
            if form.$invalid
                msgbox.error 'errors.form_validation_error'
                return

            onSuccess = (msg, exhibition) ->
                msgbox.success msg
                $uibModalInstance.close(exhibition)

            onError = (msg) ->
                msgbox.error msg
                $uibModalInstance.dismiss('close')

            if $scope.exhibition
                editExhibition($scope.data, onSuccess, onError)
            else
                newExhibition($scope.data, onSuccess, onError)

            return

        $scope.uploadFiles = (exhibition_no, file, error_files) ->
            if file
                onSuccess = (response) ->
                    delete $scope.data['attachment_no']
                    if response.data and response.data.attachment_no
                        $scope.data.attachment_no = response.data.attachment_no
                        $scope.data.image_url = response.data.attachment_url
                        $scope.data.attachment_name = file.name
                onError = (->)

                data =
                    attachmentTypeName: attachmentTypeName
                upload_url = constants.ATTACHMENT_UPLOAD_URL
                uploadFiles upload_url, data, file, onSuccess, onError

        $log.info 'modalExhibitionFormController ->'

        delete $scope['exhibition']

        if exhibition
            $scope.exhibition = exhibition

            parseToDate = (dateString) ->
                dt = moment(dateString)
                return dt.toDate()

            $scope.data =
                title: exhibition.exhibition_title
                start_date: parseToDate(exhibition.exhibition_period_begin)
                end_date: parseToDate(exhibition.exhibition_period_end)
                attachment_no: exhibition.banner
                image_url: exhibition.exhibition_banner_url
]