module.exports = [
    '$scope', '$sce', '$log', '$uibModalInstance', 'api', 'UserService', 'LikeHubService', 'ChatHubService', 'public_wall'
    ($scope, $sce, $log, $uibModalInstance, api, UserService, LikeHubService, ChatHubService, public_wall) ->
        $scope.chat_guid = ''
        $scope.message_list = []
        $scope.message_loaded = false
        $scope.scroll_to_bottom = false
        $scope.data = {}

        $scope.wall = public_wall
        $scope.user = UserService.get()

        LikeHubService.bind('updateLikeCount', (count) ->
            $scope.wall.shared_item_like_count = count
        )

        getChatGuid = (shared_item_no) ->
            onSuccess = (response) ->
                $scope.chat_guid = response.chat_guid
            onError = (->)

            data = {
                shareditem_no: shared_item_no
            }
            api.getChatGroupsFromChat data, onSuccess, onError

        $scope.parseHTML = (str) ->
            if str
                return $sce.trustAsHtml(str.replace(/(?:\r\n|\r|\n)/g, '<br />'))
            return str

        $scope.postLike = (wall_item) ->
            type = 'SharedItem'
            data_id = wall_item.shared_item_no

            onSuccess = (response) ->
                wall_item.shared_item_is_like = response.isLike
                wall_item.shared_item_like_count = response.likeCount

            onError = (->)

            api.postLike type, data_id, onSuccess, onError

        $(()->
            $('.pannable-image').ImageViewer()
        )

        getChatGuid public_wall.shared_item_no

        LikeHubService.listen 'shareditem', public_wall.shared_item_no
]