constants = require('../common/constants')

exports.ngEnter = () ->
    restrict: 'A'
    link: (scope, element, attrs) ->
        element.bind 'keydown keypress', (event) ->
            if event.which == 13
                scope.$apply ->
                    scope.$eval attrs.ngEnter

                event.preventDefault()

exports.pdRemoveClassWhenReady = ['$timeout', ($timeout) ->
    restrict: 'A'
    link: (scope, element, attrs) ->
        $contentObj = $(attrs.pdRemoveClassWhenReady)
        className = attrs.pdRemoveClassWhenReadyClassName

        $timeout(() ->
            $contentObj.removeClass(className)
        )
]

exports.pdChatAtUsers = ['$timeout', '$log', ($timeout, $log) ->
    restrict: 'A'
    scope:
        visible: '=pdChatAtUsers'
    link: (scope, element) ->
        $element = $(element)
        $textarea = $element.find('.chat-input textarea')
        $users = $element.find('.chat-users')

        adjustPosition = () ->
            top = $users.outerHeight() * -1
            left = $textarea.outerWidth() - $users.outerWidth()
            #$users.css({ 'margin-top': top + 'px', 'margin-left': left + 'px' })
            $users.css({ 'margin-top': top + 'px' })

        setVisible = (value) ->
            scope.$apply(() ->
                scope.visible = value
                $timeout(() ->
                    if value == true
                        adjustPosition()
                )
            )

        $textarea.on('keydown', (e) ->
            code = e.keyCode || e.which

            if code in [8, 27, 32] # backspace & escape & space
                setVisible false
        )
        $textarea.on('keypress', (e) ->
            code = e.keyCode || e.which

            if code == 64 # @
                setVisible true
        )
]

exports.pdScrollToBottom = ['$timeout', '$log', ($timeout, $log) ->
    restrict: 'A'
    scope:
        active: '=pdScrollToBottom'
    link: (scope, element, attrs) ->
        runScroll = () ->
            $timeout(() ->
                element[0].scrollTop = element[0].scrollHeight
                scope.active = false
            )

        watchStatement = () ->
            return scope.active
        scope.$watch(watchStatement, (value) ->
            if value == true
                runScroll()
        )
]

exports.pdSignalrHubScript = () ->
    restrict: 'E'
    link: (scope, element) ->
        s = document.createElement('script')
        s.src = constants.SIGNALR_URL + '/hubs'
        s.type = 'application/javascript'
        element.append(s)

exports.saveEnter = () ->
    restrict: 'A'
    link: (scope, element, attrs) ->
        commandKey = 17 # ctrl
        enterKey = 13 # enter
        lastKey = 0

        element.bind('keydown', (event) ->
            if event.which != commandKey && event.which != enterKey
                lastKey = 0
            if lastKey == commandKey && event.which == enterKey
                scope.$apply(() ->
                    scope.$eval(attrs.saveEnter)
                )
                event.preventDefault()
            lastKey = event.which
        )

exports.focusIf = ['$timeout', ($timeout) ->
    restrict: 'A'
    link: (scope, element, attrs) ->
        dom = element[0]

        setFocus = (dom) ->
            $dom = $(dom)
            value = $dom.val()
            $dom.focus().val('').val(value)

        focus = (condition) ->
            if condition
                delay = scope.$eval(attrs.focusDelay) || 0
                $timeout(() ->
                    #dom.focus()
                    setFocus(dom)
                , delay)
        if attrs.focusIf
            scope.$watch attrs.focusIf, focus
        else
            focus true
]