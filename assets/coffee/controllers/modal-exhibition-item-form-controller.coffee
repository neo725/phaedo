constants = require('../common/constants')

module.exports = ['$scope', '$log', '$uibModalInstance', 'Upload', 'UserService', 'msgbox', 'api', 'exhibition_no', 'exhibition_item',
    ($scope, $log, $uibModalInstance, Upload, UserService, msgbox, api, exhibition_no, exhibition_item) ->
        attachmentTypeName = 'exhibitionItem'

        $scope.data = {}

        if exhibition_item
            $scope.data =
                'title': exhibition_item.exhibition_item_name
                'description': exhibition_item.exhibition_item_description

            if exhibition_item.exhibition_item_attachment.length > 0
                attachment = exhibition_item.exhibition_item_attachment[0]
                $scope.data.attachment_no = attachment.attachment_no
                $scope.data.attachment_file_no = attachment.attachment_file_no
                $scope.data.attachment_name = attachment.attachment_file_name
                $scope.data.image_url = attachment.attachment_url

        uploadFiles = (url, data, file, onSuccess, onError) ->
            user = UserService.get()

            if file
                headers = {}
                if user
                    headers.token = user.token

                file.upload = Upload.upload(
                    url: url
                    data: data
                    file: file
                    headers: headers
                )

                file.upload.then (response) ->
                    onSuccess(response)
                , (response) ->
                    if response.status > 0
                        onError(response)
                , (evt) ->
                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total))

        deleteFileAttachment = (attachment_no, success_fn) ->
            onSuccess = () ->
                success_fn()
            onError = (->)

            api.deleteExhibitionItemAttachment(attachment_no, onSuccess, onError)

        $scope.submitForm = (form) ->
            if form.$invalid
                return

            onSuccess = () ->
                delete $scope.data['attachment_no']

                onSuccess = (response) ->
                    $uibModalInstance.close(response)

                onError = (msg) ->
                    msgbox.error msg
                    $uibModalInstance.dismiss('close')

                data =
                    'exhibition_no': exhibition_no
                    'exhibitionItem_name': $scope.data.title
                    'exhibitionItem_description': $scope.data.description
                    'exhibitionItem_attachment': [$scope.data.attachment_file_no]

                api.updateExhibitionItem(exhibition_item.exhibition_item_no, data, onSuccess, onError)

            if $scope.data.attachment_no
                deleteFileAttachment($scope.data['attachment_no'], onSuccess)

        $scope.uploadFiles = (file, error_files) ->
            if file
                onSuccess = (response) ->
#                    bindingAttachment = (response) ->
#                        $scope.data.attachment_file_no = response.data.file_no
#                        $scope.data.image_url = response.data.file_url
#                        $scope.data.attachment_name = response.data.file_name
#
#                    if $scope.data.attachment_no
#                        onSuccess = ->
#                            delete $scope.data['attachment_no']
#                            if response and response.data
#                                bindingAttachment(response)
#                        deleteFileAttachment($scope.data['attachment_no'], onSuccess)
#                        return
#                    bindingAttachment(response)

                    $scope.data.attachment_file_no = response.data.file_no
                    $scope.data.image_url = response.data.file_url
                    $scope.data.attachment_name = response.data.file_name

                onError = (->)

                data =
                    attachmentTypeName: attachmentTypeName
                upload_url = constants.ATTACHMENT_UPLOAD_URL
                uploadFiles upload_url, data, file, onSuccess, onError
]