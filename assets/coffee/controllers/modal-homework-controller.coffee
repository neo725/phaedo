constants = require('../common/constants')

module.exports = [
    '$scope', '$log', '$uibModalInstance',
        'course', 'project', 'user', 'man', 'manscore', 'api', 'msgbox', 'Upload',
    ($scope, $log, $uibModalInstance,
        course, project, user, man, manscore, api, msgbox, Upload) ->

            $scope.man = man
            $scope.manscore = manscore

            getChatGuid = () ->
                onSuccess = (response) ->
                    $scope.chat_guid = response.chat_guid
                onError = (->)

                data = {
                    receiver: man.man_no
                }
                if manscore.manscore_type == 2000
                    data.project_no = project.project_no
                api.getChatGroupsFromChat data, onSuccess, onError

            loadPersonalHomeworkList = (man, skip_close) ->
                onSuccess = (response) ->
                    $scope.homework_list = response
#                    if not skip_close
#                        $('#modalHomework').modal('toggle')
                    return
                onError = (->)

                api.getHomeworkPersonalList man.man_no, onSuccess, onError

            uploadFiles = (url, data, file, onSuccess, onError) ->
                if file
                    $scope.upload_file = file

                    token = ''
                    if user
                        token = user.token

                    file.upload = Upload.upload(
                        url: url
                        data: data
                        file: file
                        headers: { 'token': token }
                    )

                    file.upload.then (response) ->
                        #console.log 'upload success:'
                        #console.log response
                        onSuccess(response)
                    , (response) ->
                        if response.status > 0
                            #console.log 'upload error --'
                            #console.log response
                            onError(response)
                    , (evt) ->
                        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total))

            $scope.cancel = ->
                $uibModalInstance.dismiss('cancel')

            $scope.postToPublicWall = (homework, sharing_type) ->
                if homework
                    file = homework.files[0]

                    wall =
                        wall_name: file.file_name.substr(0, file.file_name.lastIndexOf('.')) || file.file_name
                        wall_desc: ''
                        wall_man: 1
                        file_no: file.file_no,
                        sharing_type: sharing_type

                    onSuccess = () ->
                        $uibModalInstance.dismiss('close')
                        msgbox.success 'message.wall_posted'
                        return
                    onError = (->)

                    api.createPublicwall(wall, onSuccess, onError)

            $scope.uploadPersonalFiles = (man, file, error_files) ->
                $log.info man
                # onSuccess = (response) ->
                #     onSuccess = (response) ->
                #         loadPersonalHomeworkList(man, true)
                #     onError = (->)

                #     upload_url = constants.PERSONAL_FILE_UPLOAD_URL
                #     data = { homeworkpersonal_no: response.homeworkpersonal_no }
                #     uploadFiles upload_url, data, file, onSuccess, onError
                # onError = (->)

                # if file
                #     homework =
                #         course_no: course.course_no
                #         project_no: project.project_no
                #         man_no: man.man_no
                #     api.createHomeworkPersonal homework, onSuccess, onError

            #loadPersonalHomeworkList($scope.man)
            getChatGuid()
]