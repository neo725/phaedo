module.exports = ['$rootScope', '$scope', '$log', 'api',
    ($rootScope, $scope, $log, api) ->
        $scope.show_create_chatmodal = false

        $scope.closeCreateChatModal = () ->
            $scope.show_create_chatmodal = false

        $scope.create_chat_list = [
            {
                'icon': 'fa-commenting'
                'title': '私信'
                'type': 'personal'
                'active': true
            },
            {
                'icon': 'fa-comments'
                'title': '班級群聊'
                'type': 'class'
            }
        ]

        $scope.openChatTo = (room) ->
            $scope.show_create_chatmodal = false
            $rootScope.$broadcast('pd:room:chatmodal_open', room)

        $scope.getRoomList = (type) ->
            i = 0
            while i < $scope.create_chat_list.length
                $scope.create_chat_list[i].active = $scope.create_chat_list[i].type == type
                i += 1

            loadRoomList()

        loadPersonalRoomList = () ->
            onSuccess = (response) ->
                list = _.filter(response, { 'room_type': 'personal' })
                $scope.room_list = list
            onError = (->)

            api.getRoomList(onSuccess, onError)

        loadClassRoomList = () ->
            onSuccess = (response) ->
                list = _.filter(response, { 'room_type': 'class' })
                $scope.room_list = list
            onError = (->)

            api.getRoomList(onSuccess, onError)

        loadRoomList = () ->
            getRoomType = () ->
                i = 0
                while i < $scope.create_chat_list.length
                    if $scope.create_chat_list[i].active == true
                        return $scope.create_chat_list[i].type
                    i += 1
            type = getRoomType()
            switch type
                when 'personal'
                    loadPersonalRoomList()
                when 'class'
                    loadClassRoomList()

        $scope.$on('pd:visible:create_chatmodal_show', () ->
            $scope.show_create_chatmodal = true
            loadRoomList()
        )
]