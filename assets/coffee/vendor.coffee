require('jquery')
require('angular')
require('bootstrap')
require('angular-ui-router')
require('angular-ui-bootstrap')
require('angular-ui-bootstrap-tpls')
require('angular-cookies')
#require('angular-route')
require('angular-translate')
require('lodash')
require('message-box')
require('ng-file-upload')
require('ng-file-upload-shim')
require('angular-cache')
require('angulargrid')
#require('google-marker-with-label')
require('ng-sweet-alert')
require('imageviewer')
require('angular-ui-select')
require('textAngular')
require('angular-bootstrap-calendar')
require('angular-bootstrap-calendar-tpls')
require('angular-date-time-input')
