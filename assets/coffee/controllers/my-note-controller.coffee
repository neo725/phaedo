module.exports = ['$rootScope', '$scope', '$sce', '$log', '$timeout', '$uibModal', 'msgbox', 'api',
    ($rootScope, $scope, $sce, $log, $timeout, $uibModal, msgbox, api) ->

        loadNotes = () ->
            onSuccess = (response) ->
                $scope.notes = response.note_list
            onError = (->)

            api.getNotes '', onSuccess, onError

        loadNotes()

        $scope.addMyNote = () ->
            modalInstance = $uibModal.open(
                animation: true
                size: 'lg'
                backdrop: 'static'
                #windowClass: 'modal-project-info modal-project-homework'
                templateUrl: 'modalMyNote.html'
                controller: 'ModalMyNoteController'
                resolve:
                    project: ->
                        return null
                    note: ->
                        return null
            )

            modalInstance.result.then (note) ->
                $scope.notes.push note

        $scope.editMyNote = (index, note) ->
            modalInstance = $uibModal.open(
                animation: true
                size: 'lg'
                backdrop: 'static'
                #windowClass: 'modal-project-info modal-project-homework'
                templateUrl: 'modalMyNote.html'
                controller: 'ModalMyNoteController'
                resolve:
                    project: ->
                        return null
                    note: ->
                        return note
            )

            modalInstance.result.then (note) ->
                $scope.notes[index] = note

        $scope.deleteMyNote = (index, note) ->          
            onYes = ->
                onSuccess = (response) ->
                    $timeout ->
                        $scope.notes.splice index, 1
                    msgbox.success 'message.data_delete_successed'
                onError = ->
                    msgbox.error 'errors.data_delete_failure'
                api.deleteNote(note.note_uid, onSuccess, onError)
            onNo = (->)

            msgbox.confirm 'message.confirm_sure_to_delete_note', onYes, onNo

]