module.exports = ['$rootScope', '$scope', '$stateParams', '$uibModal', '$log', '$timeout', 'api',
    ($rootScope, $scope, $stateParams, $uibModal, $log, $timeout, api) ->

        exhibition_no = $stateParams.exhibition_no

        $log.info 'current exhibition_no : ' + exhibition_no

        $scope.gotoEdit = (index, exhibition_item) ->
            modalInstance = $uibModal.open(
                animation: true
                size: 'lg'
                windowClass: 'modal-exhibition-item-form'
                templateUrl: 'modalExhibitionItemForm.html'
                controller: 'ModalExhibitionItemFormController'
                resolve:
                    exhibition_item: ->
                        return exhibition_item
            )
            modalInstance.result.then (item) ->
                $timeout ->
                    if index == -1
                        $scope.exhibition_items.push item
                    else
                        $scope.exhibition_items[index] = item

        getRandomInt = (min, max) ->
            return Math.floor(Math.random() * (max - min + 1)) + min

        getRandom = (values) ->
            value = values[getRandomInt(0, values.length)]
            if value == undefined
                return values[0]
            return value

        getDummyImageUrl = (width, height) ->
            #return "https://dummyimage.com/#{width}x#{height}/333333/fff.png"
            #return "http://loremflickr.com/#{width}/#{height}/art,abstract,city,nature"
            return "https://unsplash.it/#{width}/#{height}/?random"

        dummy_width = [200, 220, 250, 280, 300, 320, 350, 380, 400, 420, 450, 480, 500]
        dummy_height = [100, 120, 150, 180, 200, 220, 250, 280, 300]

        getRandomImage = () ->
            width = getRandom(dummy_width)
            height = getRandom(dummy_height)
            return getDummyImageUrl(width, height)

        loadPublicWallList = () ->
            onSuccess = (response) ->
                response.forEach((item) ->
                    #item.image_url = getRandomImage()
                    image = item.wall_files[0]
                    image.image_url = getRandomImage()
                )
                $scope.wall = response
            onError = (->)

            api.getPublicWallList(1, 500, onSuccess, onError)

        #loadPublicWallList()

        loadExhibitionItems = (exhibition_no) ->
            onSuccess = (response) ->
                $scope.exhibition_items = response
            onError = (->)

            api.getExhibitionItems(exhibition_no, onSuccess, onError)

        loadExhibitionItems(exhibition_no)
]