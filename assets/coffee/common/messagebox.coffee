module.exports = ['$translate', 'SweetAlert', ($translate, SweetAlert) ->
    swal = (title, message, type) ->
        $translate([title, message, 'popup.ok']).then (translation) ->
            SweetAlert.swal({
                title: translation[title]
                text: translation[message]
                confirmButtonText: translation['popup.ok']
                type: type
            })
    msgbox =
        alert: (title, message, type) ->
            swal(title, message, type)
        info: (message) ->
            swal('title.msgbox_info', message, 'info')
        success: (message) ->
            swal('title.msgbox_success', message, 'success')
        error: (message) ->
            swal('title.msgbox_error', message, 'error')
        confirm: (message, onDone, onFail) ->
#            $translate([message, 'popup.yes', 'popup.no']).then (translation) ->
#                $.MessageBox({
#                    buttonDone: translation['popup.yes']
#                    buttonFail: translation['popup.no']
#                    message: translation[message]
#                }).done(onDone || (->)).fail(onFail || (->))
            $translate(['title.msgbox_confirm', message, 'popup.yes', 'popup.no']).then (translation) ->
                SweetAlert.swal({
                    title: translation['title.msgbox_confirm']
                    text: translation[message]
                    type: 'warning'
                    showCancelButton: true
                    confirmButtonText: translation['popup.yes']
                    cancelButtonText: translation['popup.no']
                }, (isConfirm) ->
                    if isConfirm
                        onDone()
                    else
                        onFail()
                )

    return msgbox
]