constants = require('../common/constants')
'use strict'

module.exports = [
    '$scope', '$log', '$uibModalInstance', 'project', 'user', 'api', 'Upload', 'UserService', 'msgbox',
    ($scope, $log, $uibModalInstance, project, user, api, Upload, UserService, msgbox) ->

        loadHomeworks = () ->
            onSuccess = (response) ->
                $scope.files = response
            onError = (->)

            api.getHomeworks(project.project_no, onSuccess, onError)

        loadHomeworks()

        loadHomeworkRequests = () ->
            onSuccess = (response) ->
                $scope.homework_requests = response
            onError = (->)

            api.getHomeworkRequests(project.project_no, onSuccess, onError)

        loadHomeworkRequests()

        uploadFiles = (url, data, file, onSuccess, onError) ->
            user = UserService.get()

            if file
                headers = {}
                if user
                    headers.token = user.token

                file.upload = Upload.upload(
                    url: url
                    data: data
                    file: file
                    headers: headers
                )

                file.upload.then (response) ->
                    onSuccess(response)
                , (response) ->
                    if response.status > 0
                        onError(response)
                , (evt) ->
                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total))

        $scope.cancel = () ->
            $uibModalInstance.dismiss('cancel') 


        $scope.uploadFiles = (file, error_files) ->
            if file
                onSuccess = (response) ->
                    if response                        
                        onSuccess = (response2) ->
                            $scope.files.push response2.files[0]
                            #$log.info response2
                        onError = (->)
                        
                        file_guid = []
                        file_guid.push response.data.file_guid

                        data =
                            'project_no': project.project_no
                            'team_no': null
                            'file_guid': file_guid
                        #$log.info data
                        api.uploadFileInProject(data, onSuccess, onError)
                onError = (->)
                
                data = []
                upload_url = constants.ATTACHMENT_UPLOAD_URL
                uploadFiles upload_url, data, file, onSuccess, onError

        $scope.submitHomework = (index, file) ->
            submit_homework = file.submit_homework
            if !submit_homework
                return

            $scope.files.splice index, 1

            for homework_request, index in $scope.homework_requests
                if homework_request.homework_request_no == submit_homework.homework_request_no                    
                    homework_request_index = index

            file.homework_request_no = submit_homework.homework_request_no
            # file.homework_request_name = submit_homework.homework_request_name
            # file.homework_request_type = submit_homework.homework_request_type

            onSuccess = (response) ->
                files = $scope.homework_requests[homework_request_index].files
                files.push file
                $scope.homework_requests[homework_request_index].files = files                
            onError = (error, status_code) ->
                $log.info status_code

            api.submitHomework(submit_homework.homework_request_no, file.work_no, onSuccess, onError)

        $scope.selectionChanged = (submit_homework, file) ->
            file.submit_homework = submit_homework

        # $scope.filterFn = (item) ->
        #     return item.homeworks && item.homeworks.length == 0
]