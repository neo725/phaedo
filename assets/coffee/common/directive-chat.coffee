module.exports = [
    '$sce', '$timeout', '$log', 'api', 'ChatHubService', 'UserService',
    ($sce, $timeout, $log, api, ChatHubService, UserService) ->
        restrict: 'E'
        scope:
            chat_guid: '=guid'
            message_count: '=count'
        templateUrl: './partial/_chat-directive.html'
        link: (scope, element, attrs) ->
            scope.data = {}
            scope.message_loaded = false
            scope.user = UserService.get()
            scope.data.show_chat_users = false
            scope.data.focus_in_message = false

            loadUsersInChatroom = (chat_guid) ->
                onSuccess = (response) ->
                    scope.user_list = response

                onError = (->)

                api.getUsersInChatroom chat_guid, onSuccess, onError

            loadMessage = (guid) ->
                scope.data.show_chat_users = false
                scope.message_loaded = false
                scope.guid = guid

                ChatHubService.joinGroup guid

                onSuccess = (response) ->
                    scope.message_list = response
                    scope.message_loaded = true
                    scope.scroll_to_bottom = true

                onError = (->)

                api.getChatMessageHistoryFromChat(guid, onSuccess, onError)
                loadUsersInChatroom(guid)

            scope.parseMessageOfYou = (message, man_name) ->
                if not message
                    return false
                return message.indexOf('@' + man_name) > -1

            scope.parseHTML = (str) ->
                if str
                    return $sce.trustAsHtml(str.replace(/(?:\r\n|\r|\n)/g, '<br />'))
                return str

            scope.addUser = (man) ->
                scope.data.show_chat_users = false

                man_name = man.man_name
                scope.data.message += man_name + ' '
                scope.data.focus_in_message = true

            scope.sendMessage = (message) ->
                ChatHubService.sendMessage message, scope.guid
                scope.data.message = ''


            ChatHubService.bind('directive-chat', 'receiveMessage', (chat_guid, message) ->
                if chat_guid == scope.guid
                    $log.info 'receiveMessage from directive-chat'
                    scope.$apply(() ->
                        scope.message_list.push message
                        scope.scroll_to_bottom = true

                        if scope.message_count
                            scope.message_count = scope.message_list.length
                    )
            )

            watchStatement = ->
                return scope.chat_guid
            scope.$watch watchStatement, (guid) ->
                if guid
                    loadMessage(guid)
]