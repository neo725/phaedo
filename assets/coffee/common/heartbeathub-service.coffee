constants = require('./constants')

module.exports = ['$rootScope', 'Hub', '$timeout', '$log',
    ($rootScope, Hub, $timeout, $log) ->
        _self = {}
        _self.connect_state = 'disconnected'
        _self.server_methods = ['keepAlive']

        connect = (listeners, state_listeners) ->
            if _self.hub
                _self.hub.connect()
                return

            _self.listeners = {}
            for propertyName of listeners
                _self.listeners[propertyName] = do (propertyName) ->
                    ->
                        listeners[propertyName].apply listeners[propertyName], arguments
                        #$rootScope.$apply()
                        return

            _self.hub = new Hub('heartbeatHub', {
                # client side method
                listeners: _self.listeners

                # server side method
                methods: _self.server_methods

                # handle connection error
                errorHandler: (error) ->
                    $log.error error

                # specify a non default root
                rootPath: constants.SIGNALR_URL

                stateChanged: (state) ->
                    switch state.newState
                        when $.signalR.connectionState.connecting
                            _self.connect_state = 'connecting'
                            console.log 'signalr state : connecting...'
                            $timeout ->
                                state_listeners.connecting()
                                $rootScope.$apply()
                                return
                        when $.signalR.connectionState.connected
                            _self.connect_state = 'connected'
                            console.log 'signalr state : connected'
                            $timeout ->
                                state_listeners.connected()
                                $rootScope.$apply()
                                return
                        when $.signalR.connectionState.reconnecting
                            _self.connect_state = 'reconnecting'
                            console.log 'signalr state : reconnecting...'
                            $timeout ->
                                state_listeners.reconnecting()
                                $rootScope.$apply()
                                return
                        when $.signalR.connectionState.disconnected
                            _self.connect_state = 'disconnected'
                            console.log 'signalr state : disconnected'
                            $timeout ->
                                state_listeners.disconnected()
                                $rootScope.$apply()
                                return
            })

            return _self.hub

        disconnect = () ->
            if _self.hub
                _self.hub.disconnect()

        bind = (name, fn) ->
            whenHubReady = () ->
                if _self.hub
                    _self.hub.on(name, fn)
                    return
                $timeout whenHubReady
            whenHubReady()

        getConnectState = () ->
            return _self.connect_state

        keepAlive = (token) ->
            _self.hub.keepAlive(token)

        return {
            'getConnectState': getConnectState
            'connect': connect
            'disconnect': disconnect
            'bind': bind

            'keepAlive': keepAlive
        }
]