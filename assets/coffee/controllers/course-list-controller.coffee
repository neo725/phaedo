module.exports = ['$rootScope', '$scope', '$location', '$state', 'api', 'UserService', 'CourseService',
    ($rootScope, $scope, $location, $state, api, UserService, CourseService) ->

        $scope.state_name = $state.current.name

        user = UserService.get()

        if not user
            return

        $scope.setCurrentCourse = (course) ->
            CourseService.set course
            $state.go 'home.project-list'

        $scope.goCurrent = () ->
            $state.go 'home.course-list'

        $scope.goArchive = () ->
            $state.go 'home.archive'

        loadCourseList = (archive) ->
            filter_type = (list, type_no) ->
                return _.filter(list, { course_type_no: type_no })
            onSuccess = (response) ->
                $scope.items = response
            onError = (->)

            api.getCourseList(archive, onSuccess, onError)
            #api.getCourseListByType onSuccess, onError

        if $state.current.name == 'home.course-list'
            loadCourseList(false)
        else if $state.current.name == 'home.archive'
            loadCourseList(true)
]