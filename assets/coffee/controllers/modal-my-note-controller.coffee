module.exports = [
    '$scope', '$log', '$uibModalInstance', 'project', 'note', 'api', 'msgbox',
    ($scope, $log, $uibModalInstance, project, note, api, msgbox) ->

        if note
            data =
                'note_uid': note.note_uid
                'note_title': note.note_title
                'note_description': note.note_description
            $scope.note = data
            $scope.isNew = false
        else
            $scope.isNew = true

        if project
            project_no = project.project_no
        else
            project_no = null

        $scope.cancel = () ->
            $uibModalInstance.dismiss('cancel') 

        $scope.submitForm = (form) ->
            if form.$invalid
                return
            
            onSuccess = (response) ->
                if note
                    $uibModalInstance.close(data)
                else
                    $uibModalInstance.close(response)
            onError = (->)

            data =
                'note_uid': $scope.note.note_uid
                'note_title': $scope.note.note_title
                'note_description': $scope.note.note_description
            
            if !note
                api.createNote(project_no, data, onSuccess, onError)
            else
                api.updateNote($scope.note.note_uid, data, onSuccess, onError)
]