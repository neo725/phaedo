module.exports = ['$window', ($window) ->
    KEY = 'Phaedo.CurrentProject'

    set = (data) ->
        $window.sessionStorage.setItem(KEY, JSON.stringify(data))

    get = ->
        data = $window.sessionStorage.getItem(KEY)
        if (data)
            data = JSON.parse(data)
        return data

    return {
        get: get
        set: set
    }
]