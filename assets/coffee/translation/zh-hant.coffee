module.exports = ->
    'input': {
    }
    'text': {
    }
    'errors': {
        'form_validation_error': '画面上有栏位未完成输入或是格式有误，请检查标记为红色的栏位资料是否正确'
        'data_update_failure': '更新失败'
        'data_create_failure': '新增失败'
        'data_delete_failure': '刪除失敗'
    }
    'popup': {
        'ok': '确 定'
        'cancel': '取 消'
        'yes': '是'
        'no': '否'
    }
    'message': {
        'wall_posted': '已经张贴至分享墙'
        'data_update_successed': '更新完成'
        'data_create_successed': '新增完成'
        'data_delete_successed': '刪除完成'
        'confirm_sure_to_delete_exhibition': '確定要刪除展廳?'
        'confirm_sure_to_delete_exhibition_item': '確定要刪除展廳項目?'
        'confirm_sure_to_delete_project_team': '確定要刪除分組?'
        'confirm_sure_to_delete_calendar_task_item': '確定要删除日曆項目?'
    }
    'title': {
        'msgbox_info': '提示'
        'msgbox_success': '成功'
        'msgbox_warning': '注意'
        'msgbox_error': '發生錯誤'
        'msgbox_confirm': '確認'
    }
