module.exports = ['$rootScope', '$scope', '$timeout', '$log', 'api', 'ChatHubService', 'UserService',
    ($rootScope, $scope, $timeout, $log, api, ChatHubService, UserService) ->
        $scope.show_notify = false
        $scope.loading_notify_group = false
        $scope.notify_group = null
        $scope.message_list = []

        $scope.notify_list = [
            {
                name: '施堯仁',
                message: '456789',
                time: '2017/4/21 23:59:59'
            },
            {
                name: '施堯仁',
                message: '123',
                time: '2017/4/21 23:59:59'
            },
            {
                name: '施堯仁',
                message: 'ABCDEFG',
                time: '2017/4/21 23:59:59'
            }
        ]

        $scope.parseChatListTime = (time) ->
            return moment(time).format('YYYY/MM/DD HH:mm')

        ChatHubService.bind('notify-modal-controller', 'receiveNotification', (chat_guid, message) ->
            $log.info 'receiveNotification'
            $scope.$apply(() ->
                chat = $scope.notify_group

                message.readed = false
                #chat.unreadCount += 1

                if not chat.message_list
                    chat.message_list = []

                chat.message_list.push message
                #chat.message_list.splice 0, 0, message

                $scope.message_list = chat.message_list
                $scope.scroll_to_bottom = true
                markAsReaded(chat)

                Push.create("斐多 phaedo", {
                    body: "有新的通知",
                    icon: 'images/icon-bell.png',
                    timeout: 10000,
                    onClick: () ->
                        window.focus()
                        this.close()
                })
                #updateChatListLastMessage(chat_guid, message)
            )
        )

        favicon = new Favico({
            animation: 'slide',
            fontStyle: 'normal'
        })

        changeIconBadge = (number) ->
            $rootScope.$broadcast('pd:notify:changeIconBadge', number)
            favicon.badge(number)

        loadMessageFromChat = (chat) ->
            $scope.message_list = chat.message_list
            $scope.message_loaded = true
            $scope.scroll_to_bottom = true
            markAsReaded(chat)

        markAsReaded = (chat) ->
            unread_messages = []
            i = 0
            while i < chat.message_list.length
                message = chat.message_list[i]
                if message.readed == false
                    unread_messages.push message.chatMessage_id
                i += 1

            onSuccess = (->)

            onError = (->)

            changeIconBadge(unread_messages.length)

            if unread_messages.length > 0
                api.markChatMessageAsReaded unread_messages, onSuccess, onError

        updateUnreadState = () ->
            chat = $scope.notify_group
            i = 0
            while i < chat.message_list.length
                message = chat.message_list[i]
                if message.readed == false
                    message.readed = true
                i += 1
            changeIconBadge(0)

        getChatMessageHistory = (chat, func) ->
            onSuccess = (response) ->
                chat.message_list = response
                func(chat)

            onError = (->)

            api.getChatMessageHistory(chat.chat_guid, onSuccess, onError)

        # get push chatgroup
        getPushChatGroup = (user) ->
            if not user
                return
            onSuccess = (response) ->
                $scope.loading_notify_group = false
                $scope.notify_group = response
                getChatMessageHistory $scope.notify_group, loadMessageFromChat
            onError = ->
                $scope.loading_notify_group = false

            $scope.loading_notify_group = true
            $scope.notify_group = null
            api.getPushChatGroup onSuccess, onError

        connectToNotifyGroup = () ->
            waitNotifyGroupReady = () ->
                if $scope.notify_group
                    ChatHubService.joinGroup $scope.notify_group.chat_guid
                    return
                $timeout waitNotifyGroupReady
            waitNotifyGroupReady()

        requestPushPermission = () ->
            onGranted = () ->
                $log.info 'granted'
            onDenied = () ->
                $log.info 'denied'
            Push.Permission.request(onGranted, onDenied)

        requestPushPermission()

        $scope.$on('pd:visible:notifiy_show', () ->
            $scope.show_notify = !$scope.show_notify
            if $scope.show_notify
                $timeout(updateUnreadState, 3000)
        )
#        $scope.$on '$stateChangeSuccess', () ->
#            user = UserService.get()
#            if user == null
#                return
#            if $scope.loading_notify_group
#                return
#            if $scope.notify_group
#                return
#            getPushChatGroup(user)

        watchHubStateStatement = () ->
            return ChatHubService.getConnectState()
        $scope.$watch watchHubStateStatement, (value) ->
            if value == 'connected'
                user = UserService.get()
                getPushChatGroup(user)
                connectToNotifyGroup()
]