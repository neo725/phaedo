module.exports = [
    '$scope', '$timeout', '$log', '$uibModal', 'api', 'msgbox', 'calendarConfig',
    ($scope, $timeout, $log, $uibModal, api, msgbox, calendarConfig) ->
        calendar = {}

        calendar.calendarView = 'month'
        calendar.viewDate = new Date

        $scope.status =
            calendar_isopen: false

        getActions = (color) ->
            return [
                {
                    label: '<i class=\'glyphicon glyphicon-pencil\' style="color: ' + color.primary + '"></i>'
                    onClick: (args) ->
                        modalInstance = $uibModal.open(
                            animation: true
                            size: 'md'
                            windowClass: 'modal-calendar-create-form',
                            templateUrl: 'modalCreateEvent.html'
                            controller: ['$scope', '$log', '$uibModalInstance', 'api', ($scope, $log, $uibModalInstance, api) ->
                                event = args.calendarEvent

                                $scope.data =
                                    title : event.title
                                    description : event.description
                                    datetime: event.startsAt

                                $scope.submitForm = (form) ->
                                    if form.$invalid
                                        msgbox.error 'errors.form_validation_error'
                                        return

                                    deadline = moment($scope.data.datetime)

                                    onSuccess = (response) ->
                                        $uibModalInstance.close(response)

                                    onError = (->)

                                    api.updateTask(event.id,
                                        $scope.data.title, $scope.data.description, deadline, onSuccess, onError)

                                $scope.onTimeSet = (newDate, oldDate) ->
                                    $scope.status.calendar_isopen = false
                            ]
                        )

                        modalInstance.result.then (task) ->
                            index = _.findIndex(calendar.events, { id: task.id })
                            if index > -1
                                calendar.events.splice index, 1
                                calendar.addEvent task.id, task.title, task.description, task.deadline
                }
                {
                    label: '<i class=\'glyphicon glyphicon-remove\' style="color: ' + color.primary + '"></i>'
                    onClick: (args) ->
                        onYes = ->
                            event = args.calendarEvent
                            
                            onSuccess = (response) ->
                                index = _.findIndex calendar.events, { id: event.id }
                                if index > -1
                                    calendar.events.splice index, 1

                            onError = (->)

                            api.deleteTask(event.id, onSuccess, onError)

                        onNo = (->)

                        msgbox.confirm 'message.confirm_sure_to_delete_calendar_task_item', onYes, onNo
                }
            ]

        calendar.events = [
            {
                title: 'An event'
                color: calendarConfig.colorTypes.info
                startsAt: moment().toDate()
                #endsAt: moment().toDate()
                draggable: true
                resizable: false
                #actions: actions
            }
            {
                title: '<i class="glyphicon glyphicon-asterisk"></i> <span class="text-primary">Another event</span>, with a <i>html</i> title'
                color: calendarConfig.colorTypes.warning
                #startsAt: moment().subtract(1, 'day').toDate()
                startsAt: moment().startOf('day').toDate()
                #endsAt: moment().startOf('day').toDate()
                draggable: true
                resizable: false
                #actions: actions
            }
            {
                title: 'This is a really long event title that occurs on every year'
                color: calendarConfig.colorTypes.important
                startsAt: moment().startOf('day').add(7, 'hours').toDate()
                #endsAt: moment().startOf('day').add(7, 'hours').toDate()
                recursOn: 'year'
                draggable: true
                resizable: false
                #actions: actions
            }
            {
                title: 'This is a really special event title that occurs on every year'
                color: calendarConfig.colorTypes.special
                startsAt: moment().startOf('day').add(15, 'hours').toDate()
                #endsAt: moment().startOf('day').add(15, 'hours').toDate()
                recursOn: 'year'
                draggable: true
                resizable: false
                #actions: actions
            }
        ]

        calendar.events = [] # clear events for calendar
        calendar.cellIsOpen = true

        calendar.addEvent = (id, title, description, deadline) ->
            calendar.events.push
                id: id
                title: title
                description: description
                startsAt: moment(deadline).toDate()
                #endsAt: moment(task.deadline).endOf('day').toDate()
                #startsAt: new Date(task.deadline)
                color: calendarConfig.colorTypes.info
                actions: getActions(calendarConfig.colorTypes.info)
                draggable: false
                resizable: false
            return

        calendar.eventClicked = (event) ->
            #alert.show 'Clicked', event
            $log.info 'Clicked'
            $log.info event
            return

#        calendar.eventEdited = (event) ->
#            #alert.show 'Edited', event
#            $log.info 'Edited'
#            $log.info event
#            return
#
#        calendar.eventDeleted = (event) ->
#            #alert.show 'Deleted', event
#            $log.info 'Deleted'
#            $log.info event
#            return

        calendar.eventTimesChanged = (event) ->
            $timeout(() ->
                calendar.viewDate = event.startsAt
            )
            return

        calendar.toggle = ($event, field, event) ->
            $log.info 'toggle'
            $event.preventDefault()
            $event.stopPropagation()
            event[field] = !event[field]
            return

        calendar.timespanClicked = (date, cell) ->
            $log.info 'timespanClicked'

            if calendar.calendarView == 'month'
                if calendar.cellIsOpen and moment(date).startOf('day').isSame(moment(calendar.viewDate).startOf('day')) or cell.events.length == 0 or !cell.inMonth
                    calendar.cellIsOpen = false
                else
                    calendar.cellIsOpen = true
                    calendar.viewDate = date
            else if calendar.calendarView == 'year'
                if calendar.cellIsOpen and moment(date).startOf('month').isSame(moment(calendar.viewDate).startOf('month')) or cell.events.length == 0
                    calendar.cellIsOpen = false
                else
                    calendar.cellIsOpen = true
                    calendar.viewDate = date

        loadUserTasks = () ->
            calendar.events = []
            onSuccess = (response) ->
                _.forEach(response, (task) ->
                    calendar.addEvent task.id, task.title, task.description, task.deadline
                )
            onError = (->)
            api.getTasks(onSuccess, onError)

        $scope.openCreateEvent = () ->
            modalInstance = $uibModal.open(
                animation: true
                size: 'md'
                windowClass: 'modal-calendar-create-form',
                templateUrl: 'modalCreateEvent.html'
                controller: ['$scope', '$log', '$uibModalInstance', 'api', ($scope, $log, $uibModalInstance, api) ->
                    $scope.data = {}

                    $scope.submitForm = (form) ->
                        if form.$invalid
                            msgbox.error 'errors.form_validation_error'
                            return

                        deadline = moment($scope.data.datetime)

                        onSuccess = (response) ->
                            $uibModalInstance.close(response)

                        onError = (->)

                        api.createTask($scope.data.title, $scope.data.description, deadline, onSuccess, onError)

                    $scope.onTimeSet = (newDate, oldDate) ->
                        $scope.status.calendar_isopen = false
                ]
            )

            modalInstance.result.then (task) ->
                calendar.addEvent task.id, task.title, task.description, task.deadline

        $scope.calendar = calendar
        $scope.$on('pd:visible:calendar_show', () ->
            $scope.show_calendar = !$scope.show_calendar

            if $scope.show_calendar
                loadUserTasks()
        )
]