module.exports = ['$rootScope', '$scope', '$window', '$state', 'UserService', 'CourseService', 'ManscoreService', 'ProjectService', 'api',
    ($rootScope, $scope, $window, $state, UserService, CourseService, ManscoreService, ProjectService, api) ->
        $scope.manscore = {}
        $scope.show_create_project = false
        $scope.show_create_project_by_student = false

        $scope.project_list = []
        $scope.new_project = {}

        course = CourseService.get()
        $scope.course = course


        $scope.filter_project = (list, type, status) ->
            if status
                return _.filter(list, { project_status: status, project_type: type })
            return _.filter(list, { project_type: type })

        $scope.showCreateProject = (visible) ->
            if visible == false
                return $scope.show_create_project = false
            $scope.show_create_project = true

        $scope.showCreateProjectByStudent = (visible) ->
            if visible == false
                return $scope.show_create_project_by_student = false
            $scope.show_create_project_by_student = true

        $scope.submitForm = (form) ->
            $scope.show_create_project = false
            project =
                course_no: course.course_no
                project_name: $scope.new_project.project_name
                project_startdate: $scope.new_project.project_start
                project_enddate: $scope.new_project.project_end

            onSuccess = (response) ->
                $scope.project_list.push response

            onError = (->)

            api.createProject project, onSuccess, onError

        $scope.submitFormByStudent = (form) ->
            $scope.show_create_project_by_student = false
            project =
                course_no: course.course_no
                project_name: $scope.new_project.project_name
                project_start: $scope.new_project.project_start
                project_end: $scope.new_project.project_end

            onSuccess = (response) ->
                $scope.project_list.push response

            onError = (->)

            api.createProject project, onSuccess, onError

        $scope.setProject = (project) ->
            ProjectService.set project
            $state.go 'home.project-info'

        loadProjectList = (course_no) ->
            onSuccess = (response) ->
                $scope.project_list = response
            onError = (->)

            api.getProjectList(course_no, onSuccess, onError)

        loadManscore = (course_no) ->
            onSuccess = (response) ->
                ManscoreService.set(course_no, response)
                $scope.manscore = ManscoreService.get(course_no)

                loadProjectList(course.course_no)
            onError = (->)

            api.getManscore(course_no, onSuccess, onError)

        loadManscore(course.course_no)
]