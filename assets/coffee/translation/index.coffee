zh_hans = require('./zh-hans')()
zh_hant = require('./zh-hant')()

module.exports = ->
    {
        'zh-Hans': zh_hans,
        'zh-Hant': zh_hant
    }
