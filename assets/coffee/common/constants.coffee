module.exports = do () ->
    API_URL = window.conf.API_HOST
    #API_URL = 'http://10.168.18.144/v1' # local
    #API_URL = 'http://10.168.18.154/v1' # asp.net dev
    #API_URL = 'http://10.168.18.106/v1' # asp.net dev
    #API_URL = 'https://rd-dev.sce.pccu.edu.tw/v1' # asp.net
    #API_URL = 'http://119.29.94.75/v1' # 騰訊雲 VM
    #API_URL = 'http://localhost:8080' # JAVA Dev

    #API_URL: 'http://localhost/v1' # local
    #API_URL: 'http://10.168.18.139:8080' # java dev
    #API_URL: 'http://140.137.207.102:8080' # java scepccu
    #API_URL: 'https://rd-dev.sce.pccu.edu.tw/v1' # asp.net
    #API_URL: 'http://10.168.18.154/v1' # asp.net dev
    #API_URL: 'http://ccu.eastasia.cloudapp.azure.com/v1' # asp.net on azure
    #API_URL: 'http://119.29.94.75/v1' # 騰訊雲 VM

    #FILE_UPLOAD_URL: 'http://10.168.18.139:8080/phaedoapi/homework/upload' # java dev
    #FILE_UPLOAD_URL: 'http://140.137.207.102:8080/phaedoapi/homework/upload' # java scepccu
    #FILE_UPLOAD_URL: 'https://rd-dev.sce.pccu.edu.tw/v1/phaedoapi/homework/upload' # asp.net
    #FILE_UPLOAD_URL: 'http://10.168.18.154/v1/phaedoapi/homework/Upload' # asp.net dev
    #FILE_UPLOAD_URL: 'http://ccu.eastasia.cloudapp.azure.com/v1/phaedoapi/homework/upload' # asp.net azure
    #FILE_UPLOAD_URL: 'https://119.29.94.75/v1/phaedoapi/homework/upload' # 騰訊雲 VM
    #FILE_UPLOAD_URL: this.API_URL + '/phaedoapi/homework/upload'

    #PERSONAL_FILE_UPLOAD_URL: 'http://10.168.18.139:8080/phaedoapi/homework/personal/upload' # java dev
    #PERSONAL_FILE_UPLOAD_URL: 'http://140.137.207.102:8080/phaedoapi/homework/personal/upload' # java scepccu
    #PERSONAL_FILE_UPLOAD_URL: 'https://rd-dev.sce.pccu.edu.tw/v1/phaedoapi/homework/personal/upload' # asp.net
    #PERSONAL_FILE_UPLOAD_URL: 'http://10.168.18.154/v1/phaedoapi/homework/personal/Upload' # asp.net dev
    #PERSONAL_FILE_UPLOAD_URL: 'http://ccu.eastasia.cloudapp.azure.com/v1/phaedoapi/homework/personal/upload' # asp.net azure
    #PERSONAL_FILE_UPLOAD_URL: 'https://119.29.94.75/v1/phaedoapi/homework/personal/upload' # 騰訊雲 VM
    #PERSONAL_FILE_UPLOAD_URL: this.API_URL + '/phaedoapi/homework/personal/upload'

    #SIGNALR_URL: 'http://localhost/v1/signalr' # asp.net local
    #SIGNALR_URL: 'https://rd-dev.sce.pccu.edu.tw/v1/signalr' # asp.net dev
    #SIGNALR_URL: 'http://10.168.18.154/v1/signalr' # asp.net dev
    #SIGNALR_URL: this.API_URL + '/signalr'

    return {
        API_URL: API_URL
        ATTACHMENT_UPLOAD_URL: API_URL + '/phaedoapi/file/upload'
        FILE_UPLOAD_URL: API_URL + '/phaedoapi/homework/upload'
        PERSONAL_FILE_UPLOAD_URL: API_URL + '/phaedoapi/homework/personal/upload'
        SIGNALR_URL: API_URL + '/signalr'
        DEFAULT_LOCALE: 'zh-Hans'
    }