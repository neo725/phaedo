module.exports = [
    '$scope', '$timeout', '$uibModalInstance', 'CacheFactory', 'api',
    ($scope, $timeout, $uibModalInstance, CacheFactory, api) ->
        $scope.wxVisible = false
        $scope.qrcode_image_src = null

        if not CacheFactory.get('wxCache')
            opts =
                storageMode: 'localStorage'
            CacheFactory.createCache('wxCache', opts)
        wxCache = CacheFactory.get('wxCache')

        validateWXLogin = (state) ->
            login_in_cache = wxCache.get('login')

            if not login_in_cache
                return

            onSuccess = (response) ->
                #man_name:"张琮华"
                #school_name:"明德学院"
                #token:"kJge2Exh1EjD13xjXEGYQaZL40BQr+b8"
                wxCache.remove 'login'
                wxCache.put 'user', response

                $uibModalInstance.dismiss()
            onError = (error, status_code) ->
                $timeout ->
                    validateWXLogin state
                , 3000

            api.validateWXLogin state, onSuccess, onError

        loadDataFromCache = () ->
            data_in_cache = wxCache.get('login')
            if not data_in_cache
                $timeout loadDataFromCache, 500
                return

            $timeout ->
                validateWXLogin data_in_cache.state
            , 3000
            $scope.notice_qr_image_src = data_in_cache.notice_qr_image_url
            $scope.qrcode_image_src = data_in_cache.wxLogin_qr_image_url
            $scope.wxVisible = true

        loadDataFromCache()
]