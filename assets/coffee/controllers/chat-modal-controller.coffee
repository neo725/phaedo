module.exports = ['$rootScope', '$scope', '$sce', '$timeout', '$log', 'UserService', 'api', 'ChatHubService',
    ($rootScope, $scope, $sce, $timeout, $log, UserService, api, ChatHubService) ->
        $scope.scroll_to_bottom = false
        $scope.show_chat_users = false
        $scope.show_chatmodal = false
        $scope.chat_list = []
        $scope.message_list = {}
        $scope.focus_in_message = false

        $scope.closeChatModal = () ->
            $scope.show_chatmodal = false
            ChatHubService.leaveGroups()
            clearChatListActive()

        $scope.openChat = (item) ->
            openChat item

        $scope.parseChatListTime = (time) ->
            return moment(time).format('YYYY/MM/DD HH:mm')

        $scope.sendMessage = (message) ->
            chat = getCurrentActiveChat()
            if (chat)
                ChatHubService.sendMessage message, chat.chat_guid
                $scope.message = ''

        $scope.parseHTML = (str) ->
            if str
                return $sce.trustAsHtml(str.replace(/(?:\r\n|\r|\n)/g, '<br />'))
            return str

        $scope.addUser = (man) ->
            $scope.show_chat_users = false

            man_name = man.man_name
            $scope.message += man_name + ' '
            #$('.chat-input > textarea').focus()

            $scope.focus_in_message = true

        $scope.parseMessageOfYou = (message, man_name) ->
            return message.indexOf('@' + man_name) > -1

        ChatHubService.bind('chat-modal-controller', 'receiveMessage', (chat_guid, message) ->
            $log.info 'receiveMessage from ChatModalController'
            $scope.$apply(() ->
                active_chat = getCurrentActiveChat()
                i = 0
                while i < $scope.chat_list.length
                    chat = $scope.chat_list[i]
                    if active_chat and chat.chat_guid == chat_guid
                        message.readed = false
                        if chat.chat_guid != active_chat.chat_guid
                            chat.unreadCount += 1
                        if chat.message_list
                            chat.message_list.push message
                    if active_chat and chat.chat_guid == active_chat.chat_guid
                        $scope.message_list = chat.message_list
                        $scope.scroll_to_bottom = true
                        markAsReaded(chat)
                    i += 1

                parseMessageForAtUsers(message.message)
                updateChatListLastMessage(chat_guid, message)
            )
        )

        parseMessageForAtUsers = (message) ->
            user = UserService.get()
            currentManName = '@' + user.man_name
            if message.indexOf(currentManName) > -1
                $log.info 'send push...'
                Push.create('斐多 phaedo', {
                    body: '有人在聊天室 @ 了你',
                    icon: 'images/icon-profile.png',
                    timeout: 10000,
                    onClick: () ->
                        window.focus()
                        this.close()
                })

        updateChatListLastMessage = (chat_guid, message) ->
            i = 0
            while i < $scope.chat_list.length
                chat = $scope.chat_list[i]
                if chat.chat_guid == chat_guid
                    chat.message = message.message
                    chat.time = message.createdate
                i += 1

        createChat = (item) ->
            data = {}
            switch item.room_type
                when 'personal'
                    data.receiver = item.data_no
                when 'class'
                    data.courseclass_no = item.data_no
            onSuccess = (response) ->
                $scope.chat_list.splice 0, 0, response
                ChatHubService.joinGroup response.chat_guid
                openChat(response)
            onError = (->)

            api.createChatGroups data, onSuccess, onError

        loadMessageFromChat = (chat) ->
            $scope.message_list = chat.message_list
            $scope.message_loaded = true
            $scope.scroll_to_bottom = true
            markAsReaded(chat)

        markAsReaded = (chat) ->
            unread_messages = []
            i = 0
            while i < chat.message_list.length
                message = chat.message_list[i]
                if message.readed == false
                    unread_messages.push message.chatMessage_id
                i += 1

            onSuccess = () ->
                i = 0
                while i < chat.message_list.length
                    message = chat.message_list[i]
                    if message.readed == false
                        message.readed = true
                    i += 1
                chat.unreadCount = 0
            onError = (->)

            if unread_messages.length > 0
                api.markChatMessageAsReaded unread_messages, onSuccess, onError

        openChat = (item) ->
            $scope.show_chat_users = false
            $scope.message_loaded = false
            clearChatListActive()
            index = _.findIndex $scope.chat_list, { chat_guid: item.chat_guid }
            if index == -1
                createChat item
            else
                chat = $scope.chat_list[index]
                chat.active = true
                loadUsersInChatroom(chat)

                if chat.message_list
                    loadMessageFromChat(chat)
                    return

                getChatMessageHistory(chat, loadMessageFromChat)

        loadUsersInChatroom = (chat) ->
            onSuccess = (response) ->
                $scope.user_list = response

            onError = (->)

            api.getUsersInChatroom chat.chat_guid, onSuccess, onError

        getChatMessageHistory = (chat, func) ->
            onSuccess = (response) ->
                chat.message_list = response
                func(chat)

            onError = (->)

            $scope.message_loaded = false
            api.getChatMessageHistory(chat.chat_guid, onSuccess, onError)

        getCurrentActiveChat = () ->
            i = 0
            while i < $scope.chat_list.length
                if $scope.chat_list[i].active == true
                    return $scope.chat_list[i]
                i += 1
            return undefined

        clearChatListActive = () ->
            i = 0
            while i < $scope.chat_list.length
                $scope.chat_list[i].active = false
                i += 1

        loadChatList = (func) ->
            onSuccess = (response) ->
                $scope.chat_list = response
                i = 0
                while i < $scope.chat_list.length
                    chat = $scope.chat_list[i]
                    ChatHubService.joinGroup chat.chat_guid
                    i += 1
                func()
            onError = (->)

            api.getChatGroups onSuccess, onError

        $scope.$on('pd:visible:chatmodal_show', (event, args) ->
            $scope.show_chatmodal = true
            func = () ->
                openChat args
            loadChatList(func)
        )

        $scope.$on('pd:room:chatmodal_open', (event, args) ->
            $scope.show_chatmodal = true

            func = () ->
                openChat args

            loadChatList(func)
        )

        $scope.$on('pd:signalr:chathub:connected', () ->
            chat = getCurrentActiveChat()
            if chat
                ChatHubService.joinGroup chat.chat_guid
        )
]