var gulp = require('gulp'),
    sass = require('gulp-sass'),
    spritesmith = require('gulp.spritesmith'),
    sequence = require('run-sequence'),
    rename = require('gulp-rename'),
    yargs = require('yargs').argv;

gulp.task('font', function(){
    // fontawesome
    gulp.src('app/bower_components/components-font-awesome/fonts/*')
        .pipe(gulp.dest('fonts'));
    // bootstrap
    return gulp.src('app/bower_components/bootstrap-sass/assets/fonts/**/*')
        .pipe(gulp.dest('fonts'));
});