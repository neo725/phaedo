constants = require('../common/constants')

module.exports = [
    '$scope', '$log', '$uibModalInstance', 'course', 'project', 'user', 'group', 'manscore', 'api', 'msgbox', 'Upload',
    ($scope, $log, $uibModalInstance, course, project, user, group, manscore, api, msgbox, Upload) ->
        $scope.group = group
        $scope.manscore = manscore

        getChatGuid = () ->
            onSuccess = (response) ->
                $scope.chat_guid = response.chat_guid
            onError = (->)

            data = {
                project_group_no: group.group_no
            }
            api.getChatGroupsFromChat data, onSuccess, onError

        loadHomeworkList = (group) ->
            onSuccess = (response) ->
                $scope.homework_list = response
            onError = (->)

            api.getHomeworkList group.group_no, onSuccess, onError

        uploadFiles = (url, data, file, onSuccess, onError) ->
            if file
                $scope.upload_file = file

                token = ''
                if user
                    token = user.token

                file.upload = Upload.upload(
                    url: url
                    data: data
                    file: file
                    headers: { 'token': token }
                )

                file.upload.then (response) ->
                    #console.log 'upload success:'
                    #console.log response
                    onSuccess(response)
                , (response) ->
                    if response.status > 0
                        #console.log 'upload error --'
                        #console.log response
                        onError(response)
                , (evt) ->
                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total))

        checkUserExistsInGroup = (user, group) ->
            index = _.findIndex(group.group_man, { man_no: user.man_no })
            return index != -1

        $scope.cancel = () ->
            $uibModalInstance.dismiss('cancel')

        $scope.postToPublicWall = (homework, sharing_type) ->
            if homework
                file = homework.files[0]

                wall =
                    wall_name: file.file_name.substr(0, file.file_name.lastIndexOf('.')) || file.file_name
                    wall_desc: ''
                    wall_man: 1
                    file_no: file.file_no,
                    sharing_type: sharing_type

                onSuccess = () ->
                    $uibModalInstance.dismiss('close')
                    msgbox.success 'message.wall_posted'
                    return
                onError = (->)

                api.createPublicwall(wall, onSuccess, onError)

        $scope.uploadFiles = (group, file, error_files) ->
            onSuccess = (response) ->
                onSuccess = (response) ->
                    loadHomeworkList($scope.group)
                onError = (->)

                upload_url = constants.FILE_UPLOAD_URL
                data = { homework_no: response.homework_no }
                uploadFiles upload_url, data, file, onSuccess, onError
            onError = (->)

            if file
                homework =
                    course_no: course.course_no
                    project_no: project.project_no
                    group_no: group.group_no
                api.createHomework homework, onSuccess, onError

        #loadHomeworkList($scope.group)

        $scope.user_exists_in_group = checkUserExistsInGroup(user, group)
        if manscore.manscore_type == 2000 || $scope.user_exists_in_group
            getChatGuid()
]