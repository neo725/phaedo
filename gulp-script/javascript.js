var gulp = require('gulp'),
    gutil = require('gulp-util'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    gbrowserify = require('gulp-browserify'),
    glob = require('glob'),
    es = require('event-stream'),
    rename = require('gulp-rename'),
    browserify = require('browserify'),
    transform = require('vinyl-transform'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    gif = require('gulp-if'),
    coffee = require('gulp-coffee'),
    concat = require('gulp-concat'),
    sequence = require('run-sequence'),
    yargs = require('yargs').argv;


var release = (yargs.release == 'true') ? true : false;
gutil.log('>> javascript.release = ' + release);

gulp.task('javascript', function(){
    //sequence(['vendorjs', 'appjs', 'libsjs', 'pagesjs']);
    sequence(['vendorjs-browserify', 'appjs-browserify', 'libsjs', 'pagesjs']);
    //sequence(['vendorjs-browserify', 'libsjs', 'pagesjs']);
});

gulp.task('coffeeifyjs', function () {
    return gulp.src('assets/coffee/**/*.coffee')
        .pipe(coffee({bare: true}).on('error', gutil.log))
        .pipe(gulp.dest('assets/build/coffeeify'));
});

gulp.task('vendorjs-browserify', ['coffeeifyjs'], function(cb) {
    //return browserify('./assets/build/coffeeify/vendor.js')
    //    .bundle()
    //    .pipe(source('vendor.js'))
    //    .pipe(gulp.dest('js'));

    //return gulp.src(['./assets/build/coffeeify/vendor.js']) // you can also use glob patterns here to browserify->uglify multiple files
        //.pipe(browserified)
        //.pipe(uglify())
    return browserify('./assets/build/coffeeify/vendor.js')
        .bundle()
        .pipe(source('vendor.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(gulp.dest('js'));
});

gulp.task('vendorjs', ['coffeeifyjs'], function () {
    return gulp.src(['assets/build/coffeeify/vendor.js'])
        .pipe(browserify({
            insertGlobals: false,
            //debug: !release,
            debug: false,
            shim: {
                'jquery': {
                    path: 'app/bower_components/jquery/dist/jquery.js',
                    exports: '$'
                },
                'angular': {
                    path: 'app/bower_components/angular/angular.js',
                    exports: 'angular',
                    depends: {
                        jquery: '$'
                    }
                },
                'angular-ui-router': {
                    path: 'app/bower_components/angular-ui-router/release/angular-ui-router.js',
                    exports: null,
                    depends: {
                        angular: 'angular'
                    }
                },
                'bootstrap': {
                    path: 'app/bower_components/bootstrap/dist/js/bootstrap.js',
                    exports: 'bootstrap',
                    depends: {
                        jquery: '$'
                    }
                },
                'angular-ui-bootstrap': {
                    path: 'app/bower_components/angular-bootstrap/ui-bootstrap.js',
                    exports: null,
                    depends: {
                        angular: 'angular',
                        bootstrap: 'bootstrap'
                    }
                },
                'angular-ui-bootstrap-tpls': {
                    path: 'app/bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
                    exports: null,
                    depends: {
                        angular: 'angular',
                        bootstrap: 'bootstrap'
                    }
                },
                'angular-cookies': {
                    path: 'app/bower_components/angular-cookies/angular-cookies.js',
                    exports: null,
                    depends: {
                        angular: 'angular'
                    }
                },
                'angular-route': {
                    path: 'app/bower_components/angular-route/angular-route.js',
                    exports: null,
                    depends: {
                        angular: 'angular'
                    }
                },
                'angular-translate': {
                    path: 'app/bower_components/angular-translate/angular-translate.js',
                    exports: null,
                    depends: {
                        angular: 'angular'
                    }
                },
                'lodash': {
                    path: 'app/bower_components/lodash/dist/lodash.js',
                    exports: '_'
                },
                // http://gasparesganga.com/labs/jquery-message-box/
                // https://github.com/gasparesganga/jquery-message-box
                'message-box': {
                    path: 'app/bower_components/gasparesganga-jquery-message-box/src/messagebox.js',
                    exports: null,
                    depents: {
                        jquery: '$'
                    }
                },
                'ng-file-upload-shim': {
                    path: 'app/bower_components/ng-file-upload/ng-file-upload-shim.js',
                    exports: null
                },
                'ng-file-upload': {
                    path: 'app/bower_components/ng-file-upload/ng-file-upload.js',
                    exports: null,
                    depends: {
                        angular: 'angular'
                    }
                },
                'angular-cache': {
                    path: 'app/bower_components/angular-cache/dist/angular-cache.js',
                    exports: null,
                    depends: {
                        angular: 'angular'
                    }
                },
                'angulargrid': {
                    path: 'app/bower_components/angulargrid/angulargrid.js',
                    exports: null,
                    depends: {
                        angular: 'angular'
                    }
                },
                'google-marker-with-label': {
                    path: 'app/bower_components/google-markerwithlabel/src/markerwithlabel.js',
                    exports: 'MarkerWithLabel'
                }
            }
        }))
        //.pipe(gif(release, uglify()))
        .pipe(uglify())
        .pipe(gulp.dest('js'));
});

gulp.task('appjs-browserify', ['coffeeifyjs'], function(done) {
    //glob('./assets/build/coffeeify/**/main.js', function(err, files) {
    //    if (err) done(err);
    //
    //    var tasks = files.map(function(entry) {
    //        return browserify({ entries: entry })
    //            .bundle()
    //            //.pipe(source(entry))
    //            .pipe(source('main.js'))
    //            .pipe(buffer())
    //            .pipe(sourcemaps.init())
    //            .pipe(uglify())
    //            .pipe(sourcemaps.write('map'))
    //            .pipe(gulp.dest('js'));
    //    });
    //    es.merge(tasks).on('end', done);
    //});
    return browserify({ entries: './assets/build/coffeeify/main.js', debug: true })
        .bundle()
        .pipe(source('main.js'))
        .pipe(buffer())
        //.pipe(sourcemaps.init({loadMaps: true}))
        //.pipe(uglify())
        //.pipe(sourcemaps.write('map'))
        .pipe(gulp.dest('js'));
});

gulp.task('appjs', ['coffeeifyjs'], function () {
    return gulp.src(['assets/build/coffeeify/**/main.js', 'assets/build/coffeeify/**/main-*.js'])
        .pipe(gbrowserify({
            insertGlobals: false,
            debug: !release,
            external: ['jquery', 'angular', 'angular-cookies', 'angular-route', 'angular-translate',
                'angular-ui-router',
                'lodash', 'message-box', 'ng-file-upload', 'ng-file-upload-shim', 'angular-cache',
                'angulargrid', 'google-marker-with-label',
                'angular-ui-bootstrap', 'angular-ui-bootstrap-tpls']
        }))
        .pipe(gif(release, uglify({mangle: false})))
        //.pipe(uglify())
        .pipe(gulp.dest('js'));
});

gulp.task('libsjs', function(){
    return gulp.src('assets/js/libs/**/*.js')
        .pipe(concat('libs.js'))
        //.pipe(gif(release, uglify()))
        .pipe(uglify())
        .pipe(gulp.dest('js'));
});

gulp.task('pagesjs', function(){
    return gulp.src('assets/js/pages/**/*.js')
        .pipe(concat('pages.js'))
        //.pipe(gif(release, uglify()))
        .pipe(uglify())
        .pipe(gulp.dest('js'));
});