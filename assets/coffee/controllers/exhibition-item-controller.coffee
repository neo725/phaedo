module.exports = ['$rootScope', '$scope', '$sce', '$stateParams', '$log', 'api',
    ($rootScope, $scope, $sce, $stateParams, $log, api) ->
        exhibition_no = $stateParams.exhibition_no
        exhibition_item_no = parseInt($stateParams.exhibition_item_no)

        $scope.exhibition = {}
        $scope.attachment = {}

        $rootScope.is_no_banner = true
        $rootScope.stay_on_top = true

        loadExhibitionItems = (exhibition_no, success_fn, error_fn) ->
            onSuccess = (response) ->
                $scope.exhibition = response

                index = _.findIndex(response.exhibition_items, { 'exhibition_item_no': exhibition_item_no })

                if index > -1
                    $scope.exhibition_item = response.exhibition_items[index]
                    $scope.attachment = $scope.exhibition_item.exhibition_item_attachment[0]

                    $log.info $scope.attachment

                $log.info $scope.exhibition_item

                if success_fn
                    success_fn()

            onError = () ->
                if error_fn
                    error_fn()

            api.getExhibitionItems(exhibition_no, onSuccess, onError)

        $scope.parseHTML = (str) ->
            if str
                str = str.replace(/(?:\r\n|\r|\n)/g, '<br />')
            str = $sce.trustAsHtml(str)
            return str

        loadExhibitionItems(exhibition_no)
]