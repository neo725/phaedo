module.exports = ['$rootScope', '$scope', '$window', '$log', 'api',
    ($rootScope, $scope, $window, $log, api) ->
        $scope.show_chatlist = false
        $scope.notify_badge_number = 0

        $scope.toggleNotify = ->
            $rootScope.$broadcast('pd:visible:notifiy_show')

        $scope.toggleChatList = ->
            if $scope.show_chatlist == true
                $scope.show_chatlist = false
            else
                loadChatList(() ->
                    $scope.show_chatlist = true
                )

        $scope.openChat = (item) ->
            $scope.show_chatlist = false
            $rootScope.$broadcast('pd:visible:chatmodal_show', item)

        $scope.openCreateChatModal = () ->
            $rootScope.$broadcast('pd:visible:create_chatmodal_show')

        loadChatList = (func) ->
            onSuccess = (response) ->
                $scope.chat_list = response
                func()
            onError = (->)

            api.getChatGroups onSuccess, onError

        angular.element($window).bind('click', () ->
            $scope.show_chatlist = false
            $scope.$apply()
        )

        $rootScope.$on('pd:notify:changeIconBadge', (event, number) ->
            $scope.notify_badge_number = number
        )
]