module.exports = ['$rootScope', '$scope', '$state', '$timeout', '$log', '$uibModal', 'api', 'msgbox', 'UserService', 'CacheFactory',
    ($rootScope, $scope, $state, $timeout, $log, $uibModal, api, msgbox, UserService, CacheFactory) ->

        if not CacheFactory.get('wxCache')
            opts =
                storageMode: 'localStorage'
            CacheFactory.createCache('wxCache', opts)
        wxCache = CacheFactory.get('wxCache')

    #    $scope.user = {}
        $scope.user =
            man_id: ''
            man_pwd: ''
#            man_id: 'rich'
#            man_pwd: '1234'
            #man_id: 'scepccu3'
            #man_pwd: '3456'

#        if UserService.get()
#            $location.path '/'

        login = (data) ->
            $rootScope.show_notify = false
            $rootScope.show_chatlist = false

            user =
                man_no: data.man_no
                man_name: data.man_name
                school_name: data.school_name
                token: data.token
            UserService.set user

        watchWXUser = () ->
            user_in_cache = wxCache.get('user')
            if not user_in_cache
                $timeout watchWXUser, 500
                return
            login user_in_cache
            wxCache.remove 'user'

            $state.go 'home.course-list'

        openWXLoginModal = () ->
            wxdialog = $uibModal.open(
                templateUrl: 'wxlogin.html'
                backdrop: true
                windowClass: 'modal'
                controller: 'WXLoginController'
            )
            wxdialog.result.then(() ->
                    $log.info 'success!!!'
                , () ->
                    wxCache.remove 'login'
            )

        $scope.submitLoginForm = ($form) ->
            if $form.$invalid
                msgbox.error 'errors.form_validation_error'
                return

            onSuccess = (response) ->
                login response

                $state.go 'home.course-list'
            onError = (->)

            api.login $scope.user, onSuccess, onError

        $scope.clickToWXLogin = () ->
            onSuccess = (response) ->
                #notice_qr_image_url: "http://207.46.154.124/v1/Content/wechat_dev_account_qrcode.png"
                #state: "6debdb55c57f42ec98e94464003079c6"
                #wxLogin_qr_image_url: "http://207.46.154.124/v1/phaedoapi/qrcode?state=6debdb55c57f42ec98e94464003079c6&size=200"
                data =
                    notice_qr_image_url: response.notice_qr_image_url
                    state: response.state
                    wxLogin_qr_image_url: response.wxLogin_qr_image_url
                wxCache.remove 'user'
                wxCache.put 'login', data

                openWXLoginModal()
                watchWXUser()

            onError = (->)

            api.sendWXLogin onSuccess, onError
]