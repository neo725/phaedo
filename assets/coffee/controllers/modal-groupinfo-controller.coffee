module.exports = [
    '$scope', '$uibModal', '$uibModalInstance', '$log', 'new_group', 'group_list', 'course', 'project', 'api',
    ($scope, $uibModal, $uibModalInstance, $log, new_group, group_list, course, project, api) ->
        $scope.new_group = new_group

        $scope.cancel = () ->
            $uibModalInstance.dismiss('cancel')

        $scope.openGroupManPickup = () ->
            $log.info new_group
            onSuccess = (response) ->
                manscore_list = _.filter(response, (man) ->
                    index = _.findIndex(new_group.group_man, { man_no: man.man_no })
                    delete man['checked']
                    return man.group_no == -1 && index == -1
                )
                modalInstance = $uibModal.open(
                    animation: true
                    size: 'lg'
                    windowClass: 'modal-project-groupman-pickup'
                    templateUrl: 'modalGroupManPickup.html'
                    controller: 'ModalGroupManPickupController'
                    resolve:
                        new_group: ->
                            return $scope.new_group
                        manscore_list: ->
                            return manscore_list
                )
                modalInstance.result.then (new_group) ->
                    $scope.new_group = new_group
            onError = (->)

            api.getManscoreList(course.course_no, project.project_no, onSuccess, onError)

        $scope.removeFromGroup = (group, man) ->
            index = group.group_man.indexOf(man)
            if index > -1
                group.group_man.splice index, 1

        $scope.setAsGroupLeader = (group, man) ->
            # set all man.groupman_type = 20
            _.forEach(group.group_man, (man) ->
                man.groupman_type = 20
            )
            # set man is group leader
            index = group.group_man.indexOf(man)
            if index > -1
                group.group_man[index].groupman_type = 10

        $scope.addGroup = (new_group) ->
            onSuccess = (response) ->
                group_man_list = _.forEach(new_group.group_man, (man) ->
                    man.course_no = course.course_no
                    man.project_no = project.project_no
                    man.group_no = response.group_no

                    return man
                )
                #console.log group_man_list
                onSuccess = (response) ->
                    if not group_list
                        group_list = []
                    group_list.push(response)

                    $uibModalInstance.close(group_list)
                onError = (->)

                api.createGroupMan group_man_list, onSuccess, onError
            onError = (->)

            group =
                course_no: course.course_no
                project_no: project.project_no
                group_name: new_group.name
            api.createGroup group, onSuccess, onError
]