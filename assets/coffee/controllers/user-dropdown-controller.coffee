module.exports = ['$scope', '$state', 'UserService', 'ChatHubService',
    ($scope, $state, UserService, ChatHubService) ->

        $scope.logout = ->
            UserService.clear()
            ChatHubService.disconnect()

            $state.go 'login'

        $scope.myNote = ->
            $state.go 'home.my-note'
]