module.exports = ['$rootScope', '$scope', '$state', '$uibModal', '$log', '$timeout', 'msgbox', 'api',
        'UserService', 'LikeHubService', 'ChatHubService',
    ($rootScope, $scope, $state, $uibModal, $log, $timeout, msgbox, api,
        UserService, LikeHubService, ChatHubService) ->
            $scope.show_exhibitions = false
            $scope.exhibition_loaded = false
            $scope.exhibition_no = -1
            $scope.user = UserService.get()

            $scope.exhibitions = []

            dummy_width = [200, 220, 250, 280, 300, 320, 350, 380, 400, 420, 450, 480, 500]
            dummy_height = [100, 120, 150, 180, 200, 220, 250, 280, 300]

            $scope.slickConfig =
                enabled: true
                arrows: false
                autoplay: true
                infinite: true
                #draggable: false
                autoplaySpeed: 5000
                slidesToShow: 1
                centerMode: true
                variableWidth: true

            state_listeners = {
                state: ''
                'connecting': (() ->)
                'connected': () ->
                    this.state = 'connected'
                    $log.info 'connected.'
                    $rootScope.$broadcast('pd:signalr:likehub:connected')
                'reconnecting': () ->
                    this.state = 'reconnecting'
                'disconnected': () ->
                    this.state = 'disconnected'
                    $log.info 'disconnected.'
                    $timeout(runConnect, 3000)
            }

            runConnect = () ->
                LikeHubService.connect({}, state_listeners)

            runConnect()

            getRandomInt = (min, max) ->
                return Math.floor(Math.random() * (max - min + 1)) + min

            getRandom = (values) ->
                value = values[getRandomInt(0, values.length)]
                if value == undefined
                    return values[0]
                return value

            getDummyImageUrl = (width, height) ->
                #return "https://dummyimage.com/#{width}x#{height}/333333/fff.png"
                #return "http://loremflickr.com/#{width}/#{height}/art,abstract,city,nature"
                return "https://unsplash.it/#{width}/#{height}/?random"

            getDummyExhibition = (index, size) ->
                return {
                    'exhibition_no': index
                    'image': getDummyImageUrl(size.w, size.h)
                    'title': "test title #{size.w}x#{size.h}"
                    'description': 'test description'
                }

    #        i = 0
    #        while i < getRandomInt(10, 50)
    #            width = getRandom(dummy_width)
    #            height = getRandom(dummy_height)
    #            $scope.exhibitions.push(getDummyExhibition(i, { w: width, h: height }))
    #            i++

            loadPublicWallList = () ->
                onSuccess = (response) ->
                    $scope.wall = response
                onError = (->)

                api.getPublicWallList(1, 500, onSuccess, onError)

            loadExhibitionItems = (exhibition_no, success_fn, error_fn) ->
                onSuccess = (response) ->
                    $scope.exhibition_no = exhibition_no
                    $scope.exhibition_items = response.exhibition_items

                    if success_fn
                        success_fn()

                onError = () ->
                    if error_fn
                        error_fn()

                api.getExhibitionItems(exhibition_no, onSuccess, onError)

            loadExhibitionList = () ->
                onSuccess = (response) ->
                    exhibitions = response

                    if exhibitions.length > 0
                        exhibition = exhibitions[0]
                        exhibition_no = exhibition.exhibition_no
                        $scope.exhibition = exhibition

                        onSuccess = () ->
                            $timeout(() ->
                                $scope.exhibition_loaded = true
                                $scope.show_exhibitions = true
                            , 1000)

                        loadExhibitionItems(exhibition_no, onSuccess, (->))
                onError = (->)

                api.getExhibitions(onSuccess, onError)

            $scope.gotoExhibition = (exhibition_no) ->
                #$log.info "goto exhibition_no : #{exhibition_no}"
                $state.go('public.exhibition', { exhibition_no: exhibition_no })

            $scope.gotoExhibitionItem = (exhibition_item_no) ->
                $log.info "goto exhibition_item_no : #{exhibition_item_no}"

            $scope.gotoEdit = (index, exhibition, $event) ->
                $event.stopPropagation()

                modalInstance = $uibModal.open(
                    animation: true
                    size: 'lg'
                    windowClass: 'modal-exhibition-form'
                    templateUrl: 'modalExhibitionForm.html'
                    controller: 'ModalExhibitionFormController'
                    resolve:
                        exhibition: ->
                            return exhibition
                )
                modalInstance.result.then (exhibition) ->
                    $scope.exhibition_loaded = false
                    $timeout ->
                        if index == -1
                            $scope.exhibitions.push exhibition
                        else
                            $scope.exhibitions[index] = exhibition
                        $scope.exhibition_loaded = true

            $scope.gotoItemEdit = (index, exhibition_item, $event) ->
                $event.stopPropagation()

                modalInstance = $uibModal.open(
                    animation: true
                    size: 'lg'
                    windowClass: 'modal-exhibition-item-form'
                    templateUrl: 'modalExhibitionItemForm.html'
                    controller: 'ModalExhibitionItemFormController'
                    resolve:
                        exhibition_no: ->
                            return $scope.exhibition_no
                        exhibition_item: ->
                            return exhibition_item
                )

                modalInstance.result.then (item) ->
                    $scope.exhibition_loaded = false
                    $timeout ->
                        if index == -1
                            $scope.exhibition_items.push item
                        else
                            $scope.exhibition_items[index] = item
                        $scope.exhibition_loaded = true

            $scope.gotoDelete = (index, exhibition_no, $event) ->
                $event.stopPropagation()

                onYes = ->
                    onSuccess = ->
                        $scope.exhibition_loaded = false
                        $timeout ->
                            $scope.exhibitions.splice index, 1
                            $scope.exhibition_loaded = true
                        msgbox.success 'message.data_delete_successed'
                    onError = ->
                        msgbox.error 'errors.data_delete_failure'

                    api.deleteExhibition(exhibition_no, onSuccess, onError)

                onNo = (->)

                msgbox.confirm 'message.confirm_sure_to_delete_exhibition', onYes, onNo

            $scope.gotoItemDelete = (index, exhibition_no, $event) ->
                $event.stopPropagation()

                onYes = ->
                    onSuccess = ->
                        $scope.exhibition_loaded = false
                        $timeout ->
                            $scope.exhibition_items.splice index, 1
                            $scope.exhibition_loaded = true
                        msgbox.success 'message.data_delete_successed'
                    onError = ->
                        msgbox.error 'errors.data_delete_failure'

                    api.deleteExhibitionItem(exhibitionItem_no, onSuccess, onError)
                onNo = (->)

                msgbox.confirm 'message.confirm_sure_to_delete_exhibition_item', onYes, onNo

            $scope.showPublicWallItem = (public_wall) ->
                modalInstance = $uibModal.open(
                    animation: true
                    size: 'lg'
                    windowClass: 'modal-publicwall-item'
                    templateUrl: 'modalPublicWallItem.html'
                    controller: 'ModalPublicWallItemController'
                    resolve:
                        public_wall: ->
                            return public_wall
                )
                modalInstance.result.finally(->
                    $log.info 'public-wall item modal closed'
                    LikeHubService.leaveGroup('shareditem', public_wall.shared_item_no)
                )

            $scope.postLike = (wall_item) ->
                type = 'SharedItem'
                data_id = wall_item.shared_item_no

                onSuccess = (response) ->
                    wall_item.shared_item_is_like = response.isLike
                    wall_item.shared_item_like_count = response.likeCount

                onError = (->)

                api.postLike type, data_id, onSuccess, onError

            loadExhibitionList()
            loadPublicWallList()
]