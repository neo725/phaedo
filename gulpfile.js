var gulp = require('gulp'),
    gutil = require('gulp-util'),
    clean = require('gulp-clean'),
    sequence = require('run-sequence'),
    requireDir = require('require-dir'),
    yargs = require('yargs').argv;


requireDir('./gulp-script');

gulp.task('clean', function () {
    return gulp.src(['css', 'js', 'images', 'fonts', 'assets/dist', 'assets/build'], { read: false })
        .pipe(clean({ force: true }));
});

gulp.task('watch', function () {
    gulp.watch(['assets/coffee/main.coffee', 'assets/coffee/*/*.coffee', 'assets/coffee/*/**/*.coffee'], ['appjs-browserify']);
    //gulp.watch('assets/coffee/vendor.coffee', ['vendorjs-browserify']);
    gulp.watch(['assets/scss/*.scss', 'assets/scss/**/*.scss'], ['css']);
    gulp.watch('assets/imgs/assets/*', ['image']);
    gulp.watch('assets/imgs/icons/*', ['image']);
    gulp.watch('assets/js/*', ['libsjs', 'pagesjs']);
});

gulp.task('assets', function () {
    sequence('css', 'image', 'javascript', 'font');
});

gulp.task('default', function () {
    sequence('clean', 'assets', 'watch');
});
