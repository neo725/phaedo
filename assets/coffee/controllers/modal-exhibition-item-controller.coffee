module.exports = [
    '$scope', '$sce', '$log', '$uibModalInstance', 'msgbox', 'exhibition_item',
    ($scope, $sce, $log, $uibModalInstance, msgbox, exhibition_item) ->
        #$log.info exhibition_item

        $scope.item = exhibition_item

        $scope.parseHTML = (str) ->
            if str
                str = str.replace(/(?:\r\n|\r|\n)/g, '<br />')
            str = $sce.trustAsHtml(str)
            return str
]