module.exports = [
    '$rootScope', '$scope', '$state', '$timeout', '$log', 'UserService', 'HeartbeatHubService',
    ($rootScope, $scope, $state, $timeout, $log, UserService, HeartbeatHubService) ->
        user = UserService.get()

        if not user
            $state.go 'login'
            return

        initHeartbeatHub = () ->
            listeners = {}

            state_listeners = {
                connecting: (() ->)
                connected: () ->
                    $log.info 'heartbeathub connected.'
                    $rootScope.$broadcast('pd:signalr:heartbeathub:connected')
                reconnecting: (->)
                disconnected: () ->
                    $log.info 'heartbeathub disconnected.'
                    $timeout(runConnect, 3000)
            }

            runConnect = () ->
                HeartbeatHubService.connect(listeners, state_listeners)

            runConnect()

        initHeartbeatHub()
]