module.exports = ['$window', ($window) ->
    KEY = 'Phaedo.CurrentUser'

    set = (data) ->
        $window.sessionStorage.setItem(KEY, JSON.stringify(data))
        setIsValid true

    get = ->
        data = $window.sessionStorage.getItem(KEY)
        if (data)
            data = JSON.parse(data)
        return data

    getIsLogin = ->
        user = get()
        if user
            return true
        return false

    setIsValid = (valid) ->
        $window.sessionStorage.setItem("#{KEY}.IsValid", valid)

    getIsValid = ->
        data = $window.sessionStorage.getItem("#{KEY}.IsValid")
        if data
            return data == "true" || data == true
        return true

    clear = ->
        $window.sessionStorage.removeItem(KEY)
        $window.sessionStorage.removeItem("#{KEY}.IsValid")

    return {
        get: get
        set: set
        clear: clear
        setIsValid: setIsValid
        getIsValid: getIsValid
        getIsLogin: getIsLogin
    }
]